#ifndef CAPTURETHREAD_H
#define CAPTURETHREAD_H

// Qt
#include <QtCore/QTimer>
// OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
// Local
#include "Buffer.h"
#include "config.h"
#include "structures.h"

using namespace cv;

class ImageBuffer;

class Capture : public QObject
{
    Q_OBJECT

    enum PlaybackState {
        PLAYING,
        PAUSED,
        STOPPED
    };

public:
    Capture(Buffer<Mat> *buffer, QString fileName, bool dropFrameIfBufferFull, int width, int height);
    bool connectToFile();
    bool disconnectFile();
    bool isFileConnected();
    int getInputSourceWidth();
    int getInputSourceHeight();

public slots:
    void updateFps(int fps);
    void startPlayback();
    void pausePlayback();
    void stopPlayback();

private:
    //void updateFPS(int);
    Buffer<Mat> *buffer;
    VideoCapture cap;
    Mat grabbedFrame;
    QTimer t;
    //QQueue<int> fps;
    struct ThreadStatisticsData statsData;
    volatile bool doStop;
    int captureTime;
    int sampleNumber;
    int fpsSum;
    bool dropFrameIfBufferFull;
    QString fileName;
    int width;
    int height;
    int fps;
    PlaybackState playbackState;

private slots:
    void captureFrame();

signals:
    void newFrame();
    void updateStatisticsInGUI(struct ThreadStatisticsData);
};

#endif // CAPTURETHREAD_H
