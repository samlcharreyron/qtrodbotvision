#ifndef CHAINWINDOW_H
#define CHAINWINDOW_H

//Qt
#include <QDialog>
//local
#include "chainviewer.h"

namespace Ui {
class ChainWindow;
}

class ChainWindow : public QDialog
{
    Q_OBJECT

public:
    explicit ChainWindow(QWidget *parent, Rodbot::Vision::IPChain *ipChain, Rodbot::Vision::ChainRegistry *chainRegistry);
    ~ChainWindow();

    ChainViewer *chainViewer;

private:

    Ui::ChainWindow *ui;

    bool isVertical;
    bool showIOButtons;

public slots:
    void reloadChainViewer() const;

};

#endif // CHAINWINDOW_H
