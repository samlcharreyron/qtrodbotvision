#ifndef CHAINVIEWER_H
#define CHAINVIEWER_H

#include <QScrollArea>
#include <QDebug>
#include <QDropEvent>
#include <QDrag>
#include <QMimeData>
#include <QHash>
#include <QList>
#include <QBoxLayout>
#include <QPixmap>
#include <QPainter>
#include <QSignalMapper>
#include <QMessageBox>
// IPChain
#include <IPChain/IPUnits/IPUnits.hpp>
//local
#include "config.h"
#include "units.h"
#include "ipunitcontainer.h"
#include "unitlist.h"

class ChainViewer : public QScrollArea
{
    Q_OBJECT
public:
    explicit ChainViewer(QWidget *parent,
                             Rodbot::Vision::IPChain *ipChain, Rodbot::Vision::ChainRegistry* chainRegistry,
                         UnitListModel* dialogRegistry, bool isVertical = true);

    // update ui from ipchain (used when importing chain)
    void reloadViewerFromChain();

protected:
    void mousePressEvent(QMouseEvent *event);
    void dragEnterEvent(QDragEnterEvent *e);
    void dragMoveEvent(QDragMoveEvent *e);
    void dropEvent(QDropEvent *event);

private:
    // pointer to unit registry
    UnitListModel* dialogRegistry;

    // pointer to the processing chain
    Rodbot::Vision::IPChain *ipChain;
    Rodbot::Vision::ChainRegistry* chainRegistry;

    QHash<IPUnitContainer*,Rodbot::Vision::IPUnit*> containerToChainUnit;
    QList<IPUnitContainer*> containerIndex;

    // drag/drop registry
    QHash<QString,IPUnitContainer*> containerDragDrogRegisty;

    //layout
    QBoxLayout *scrollLayout;
    QWidget   *scrollWidget;
    QBoxLayout *scrollWidgetLayout;

    QString getLabel(IPUnitContainer* container);
    void printChainLayout();
    int indexOf(QLayoutItem* item) const;

    // creates unit in specified position
    IPUnitContainer* createUnitContainer(const QString dialogName, const QString unitName, Rodbot::Vision::IPUnit* unit=NULL);

    //Rodbot::Vision::IPUnit* createUnit(QString name);

    void addUnit(const int before, IPUnitContainer* unitContainer);
    // moves unit from current position to specified position
    void moveUnit(const int newIndex, IPUnitContainer *unitContainer);

    void removeUnit(IPUnitContainer* unitContainer);

    void setUpDownButtonVisibility();

private slots:
    // moves unit up or down one position
    void moveUnit(bool moveUp);
    // removes unit from chain (destructive)
    void removeUnit();
    // sets the unit to active/inactive
    void toggleEnableUnit(bool enable);
    // applies the settings in the container to the actual processing function
    void changeSettings();

public slots:
    // add a unit based on mime data (will add to end of chain)
    void addUnit(QMimeData* mimeData);

    void handleUnitError(Rodbot::Vision::IPUnit* unit);

    // load/save configuration using file dialog
    void loadChainConfig();
    void saveChainConfig();

};

#endif // CHAINVIEWER_H
