#include "processing.h"

Processing::Processing(Buffer<Mat> *buffer, Rodbot::Vision::IPChain *ipChain) : buffer(buffer)
{
    this->ipChain = ipChain;
    doStop = false;
    sampleNumber = 0;
    fpsSum = 0;
    fps.clear();
    statsData.averageFPS = 0;
    statsData.nFramesProcessed = 0;
}

void Processing::setDoStop(const bool stop)
{
    doStop = stop;
}

void Processing::processFrame()
{
    //while (!doStop) {
        // Save processing time
        processingTime=t.elapsed();
        // Start timer (used to calculate processing rate)
        t.start();

        processingMutex.lock();
        // Get frame from queue, store in currentFrame, set ROI
        currentFrame=Mat(buffer->get().clone());


        // If there is a configuration problem causing OpenCV to
        // throw an error, the plan is to remove the problematic
        // IPUnit
        try {
            ipChain->ProcessImage(currentFrame,processedFrame);
        } catch(Rodbot::Vision::UnitError& e) {
            qDebug() << e.what();
            doStop = true;
            emit errorInUnit(e.unitAddr);
            // Wait here for the problematic unit to be removed
            waitForGoodChain.wait(&processingMutex);
        }

        // Convert Mat to QImage
        frame=MatToQImage(processedFrame);
        processingMutex.unlock();

        // Inform GUI thread of new frame (QImage)
        emit newFrame(frame);

        // Update statistics
        updateFPS(processingTime);
        statsData.nFramesProcessed++;
        // Inform GUI of updated statistics
        emit updateStatisticsInGUI(statsData);
    //}

}

void Processing::updateFPS(int timeElapsed)
{
    // Add instantaneous FPS value to queue
    if(timeElapsed>0)
    {
        fps.enqueue((int)1000/timeElapsed);
        // Increment sample number
        sampleNumber++;
    }

    // Maximum size of queue is DEFAULT_PROCESSING_FPS_STAT_QUEUE_LENGTH
    if(fps.size()>PROCESSING_FPS_STAT_QUEUE_LENGTH)
        fps.dequeue();

    // Update FPS value every DEFAULT_PROCESSING_FPS_STAT_QUEUE_LENGTH samples
    if((fps.size()==PROCESSING_FPS_STAT_QUEUE_LENGTH)&&(sampleNumber==PROCESSING_FPS_STAT_QUEUE_LENGTH))
    {
        // Empty queue and store sum
        while(!fps.empty())
            fpsSum+=fps.dequeue();
        // Calculate average FPS
        statsData.averageFPS=fpsSum/PROCESSING_FPS_STAT_QUEUE_LENGTH;
        // Reset sum
        fpsSum=0;
        // Reset sample number
        sampleNumber=0;
    }
}
