#include "mainwindow.h"
#include "ui_mainwindow.h"

QWaitCondition waitForGoodChain;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {
    ui->setupUi(this);
    connect(ui->actionOpen,SIGNAL(triggered()), this, SLOT(openVideoFile()));
    connect(ui->actionClose,SIGNAL(triggered()), this, SLOT(closeVideoFile()));
    connect(ui->actionAbout,SIGNAL(triggered()),SLOT(showAboutDialog()));
    connect(ui->actionChain,SIGNAL(triggered(bool)),SLOT(showChainWindow(bool)));
    connect(ui->actionImportChain, SIGNAL(triggered()), SLOT(loadIPChainFromFile()));
    connect(ui->actionExportChain, SIGNAL(triggered()), SLOT(saveIPChainToFile()));

    // create image buffer
    buffer  = new Buffer<Mat>(DEFAULT_BUFFER_SIZE);
    // create image processing chain
    ipChain = new Rodbot::Vision::IPChain();
    chainRegistry = new Rodbot::Vision::ChainRegistry();

    chainWindow = new ChainWindow(this,ipChain,chainRegistry);
    videoViewer = new VideoViewer(ui->viewerWidget,buffer,ipChain,chainWindow->chainViewer);
    ui->viewerWidget = videoViewer;

    connect(chainWindow,SIGNAL(rejected()),SLOT(uncheckChainWindowMenu()));
    connect(this, SIGNAL(chainSettingsLoaded()), chainWindow, SLOT(reloadChainViewer()));
}

void MainWindow::openVideoFile()
{
    // Open file dialog
    QString fileName = QFileDialog::getOpenFileName(this,tr("Open video file"),"/Users/Sam/",tr("Video Files (*.avi *.mov *.mp4);;All files (*)"));
    //QString fileName = "/Volumes/SamHD/Users/Sam/Documents/workspace/IPChainTest/test.mp4";

    if (fileName.isEmpty())
        return;
    qDebug() << "opening " << fileName;

    if (!videoViewer->connectToFile(fileName,DEFAULT_DROP_FRAMES,-1,-1))
    {
        QMessageBox::warning(this,"ERROR:","Could not open video file");
    }
}

void MainWindow::closeVideoFile()
{
    videoViewer->closeFile();
}

void MainWindow::showAboutDialog()
{
    QMessageBox::about(this,"About",QString("Created by Samuel Charreyron at ETHZ\n\nVersion: %1").arg(APP_VERSION));
}

void MainWindow::showChainWindow(bool isChecked)
{
    if (isChecked) {
        chainWindow->show();
    } else {
        chainWindow->hide();
    }
}

void MainWindow::uncheckChainWindowMenu()
{
    ui->actionChain->setChecked(false);
}

void MainWindow::loadIPChainFromFile()
{
    // Open file dialog
    QString fileName = QFileDialog::getOpenFileName(this,tr("Open IPChain configuration"),"/Users/Sam/",tr("Config files (*.yaml);;All files (*)"));
    //QString fileName = "/Volumes/SamHD/Users/Sam/Development/rodbotvision/examples/test.yaml";
    if (fileName.isEmpty())
        return;

    qDebug() << "opening " << fileName;

    ipChain->loadFromYaml(fileName.toStdString());
    emit(chainSettingsLoaded());

}

void MainWindow::saveIPChainToFile()
{
    // Open file dialog
    QString fileName = QFileDialog::getSaveFileName(this,tr("Save IPChain configuration"),"/Users/Sam/",tr("Config files (*.yaml);;All files (*)"));
    if (fileName.isEmpty())
        return;

    ipChain->saveToYaml(fileName.toStdString());

}

MainWindow::~MainWindow()
{
    if (videoViewer->isfileConnected())
        videoViewer->closeFile();
    delete ui;
    delete ipChain;
    delete chainRegistry;
}
