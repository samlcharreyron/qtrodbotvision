#include "units.h"

QDebug operator<<(QDebug dbg, const Rodbot::Vision::IPUnit &unit) {
    dbg << QString::fromStdString(unit.toString());
    return dbg;
}

QDataStream& operator<<(QDataStream &stream, const Rodbot::Vision::IPUnit &unit) {
    //stream << QString::fromStdString(unit.name()) << unit.m_settings;
    return stream;
}


QDataStream& operator>>(QDataStream &stream, Rodbot::Vision::IPUnit &unit) {
    QString qname;
    Rodbot::Vision::UnitSettings settings;
    stream >> qname;
    //stream >> settings;
    std::string name = qname.toStdString();

    unit = *(REGISTRAR.create(name));

    return stream;
}
