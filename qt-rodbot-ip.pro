#-------------------------------------------------
#
# Project created by QtCreator 2013-12-11T17:29:15
#
#-------------------------------------------------

QT       += core gui

QT_CONFIG -= no-pkg-config
CONFIG += link_pkgconfig
PKGCONFIG += opencv

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QMAKE_CXXFLAGS += -std=c++0x

TARGET = qt-rodbot-ip
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    videoviewer.cpp \
    MatToQImage.cpp \
    ipunitcontainer.cpp \
    Dialogs/smoothingdialog.cpp \
    unitlist.cpp \
    units.cpp \
    Dialogs/thresholddialog.cpp \
    Dialogs/adaptivethresholddialog.cpp \
    Dialogs/cannydialog.cpp \
    Dialogs/contoursdialog.cpp \
    processing.cpp \
    capture.cpp \
    chainwindow.cpp \
    chainviewer.cpp \
    Dialogs/morphologydialog.cpp \
    Dialogs/sobeldialog.cpp \
    Dialogs/laplaciandialog.cpp \
    Dialogs/blobdialog.cpp

HEADERS  += mainwindow.h \
    videoviewer.h \
    Buffer.h \
    MatToQImage.h \
    config.h \
    structures.h \
    ipunitcontainer.h \
    Dialogs/smoothingdialog.h \
    ipdialoginterface.h \
    unitlist.h \
    units.h \
    Dialogs/thresholddialog.h \
    Dialogs/adaptivethresholddialog.h \
    Dialogs/cannydialog.h \
    Dialogs/contoursdialog.h \
    processing.h \
    capture.h \
    chainwindow.h \
    chainviewer.h \
    Dialogs/morphologydialog.h \
    Dialogs/sobeldialog.h \
    registrar.h \
    Dialogs/laplaciandialog.h \
    Dialogs/blobdialog.h

FORMS    += mainwindow.ui \
    videoviewer.ui \
    Dialogs/smoothingdialog.ui \
    ipunitcontainer.ui \
    Dialogs/thresholddialog.ui \
    Dialogs/adaptivethresholddialog.ui \
    Dialogs/cannydialog.ui \
    Dialogs/contoursdialog.ui \
    chainwindow.ui \
    Dialogs/morphologydialog.ui \
    Dialogs/sobeldialog.ui \
    Dialogs/laplaciandialog.ui \
    Dialogs/blobdialog.ui

DEFINES += APP_VERSION=\\\"0.0.1\\\"

OTHER_FILES += \
    README.md


unix:!macx:!symbian: LIBS += -L/home/samuelch/src/rodbotvision/lib/ -lIPChain
unix:!macx:!symbian: LIBS += -L/home/samuelch/opt/lib/ -lyaml-cpp

INCLUDEPATH += /home/samuelch/src/rodbotvision/include/
INCLUDEPATH += /home/samuelch/opt/include
DEPENDPATH += /home/samuelch/opt/include

#unix:!macx:!symbian: PRE_TARGETDEPS += /home/D/samuelch/opt/lib/libIPChain.a

#unix:!macx:!symbian: LIBS += -L$$PWD/../../../opt/lib/ -lyaml-cpp

#INCLUDEPATH += $$PWD/../../../opt/include/yaml-cpp
#DEPENDPATH += $$PWD/../../../opt/include/yaml-cpp

#unix:!macx:!symbian: PRE_TARGETDEPS += $$PWD/../../../opt/lib/libyaml-cpp.a
