#ifndef UNITS_H
#define UNITS_H

#include <QString>
#include <QMetaType>
#include <QDebug>
#include <IPChain/IPChain.hpp>
#include <IPChain/ChainRegistry.hpp>
#include <IPChain/IPUnits/IPUnits.hpp>
//local
#include "config.h"

#define REGISTRAR Rodbot::Vision::ChainRegistry::get().IPUnitRegistrar
#define REG_CONST_ITERATOR Rodbot::Vision::Registrar<Rodbot::Vision::IPUnit>::const_iterator

QDebug operator<<(QDebug dbg, const Rodbot::Vision::IPUnit & unit );

//QDataStream& operator<<(QDataStream & stream, const Rodbot::Vision::IPUnit & unit);

QDataStream& operator>>(QDataStream & stream, Rodbot::Vision::IPUnit & unit);


inline QList<QString> listAllUnits() {
    QList<QString> list;

    REG_CONST_ITERATOR it = REGISTRAR.begin();

    for(; it != REGISTRAR.end(); it++) {
        list << QString::fromStdString(it->first);
    }

    return list;
}



#endif // UNITS_H
