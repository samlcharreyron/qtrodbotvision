#ifndef REGISTRAR_H
#define REGISTRAR_H

#include <QString>
#include <QMap>
#include <QDebug>
#include <IPChain/ChainRegistry.hpp>
//local
#include "ipdialoginterface.h"

class Registrar
{
protected:
    struct ClassDescriptor {

        // Create a dialog
        virtual IPDialog* createInstance() const = 0;

        // Return a label for the dialog
        virtual const QString label() const = 0;

        virtual const QString unitName() const = 0;

        virtual ~ClassDescriptor() {}
    };

    // templated on the dialog
    // IMPORTANT: T must derive from IPDialog
    template <typename T>
    struct DialogClassDescriptor : public ClassDescriptor {
        // name of the IPChain unit
        QString m_unitName;

        DialogClassDescriptor(const QString unitName) : m_unitName(unitName) {}

        IPDialog* createInstance() const {
            // create the IPUnit
            return new T();
        }

        const QString label() const {
            return T::label();
        }

        const QString unitName() const {
            return m_unitName;
        }
    };

    typedef QMap<QString, ClassDescriptor*> DescriptorMap;
    DescriptorMap classes;


    void reg(QString dialogName, ClassDescriptor* descriptor) {
        classes[dialogName] = descriptor;
    }

    ClassDescriptor* getDescriptor(const QString name) const {
        DescriptorMap::const_iterator it = classes.find(name);

        if (it == classes.end()) {
            qFatal("ERROR: trying to load unregistered dialog ");
        }

        return it.value();
    }

    // return number of registered dialogs
    int numRegistered() const {
        return classes.size();
    }

    // Dialog name
    const QString nameAt(const int pos) const {
        QStringList names = classes.keys();
        return names[pos];
    }

    const QString labelAt(const int pos) const {
        QStringList labels;
        foreach(const ClassDescriptor* descriptor, classes.values()) {
            labels << descriptor->label();
        }

        return labels[pos];
    }

    const QString unitNameAt(const int pos) const {
        QStringList unitNames;
        foreach(const ClassDescriptor* descriptor, classes.values()) {
            unitNames << descriptor->unitName();
        }

        return unitNames[pos];
    }

    const QStringList unitNames() const {
        QStringList unitNames;
        foreach(const ClassDescriptor* descriptor, classes.values()) {
            unitNames << descriptor->unitName();
        }

        return unitNames;
    }

public:

    ~Registrar() {
        foreach(ClassDescriptor* descriptor, classes.values()) {
            delete descriptor;
        }
    }

    typedef DescriptorMap::const_iterator const_iterator;

    const_iterator begin() {
        return classes.begin();
    }

    const_iterator end() {
        return classes.end();
    }

    void dump(QDebug dbg) const {
        foreach(ClassDescriptor* descriptor, classes.values()) {
            dbg << "-" << descriptor->label();
        }
    }

    // create a dialog based on the name
    IPDialog* create(const QString name) {
        qDebug() << "trying to create: " << name;
        return getDescriptor(name)->createInstance();
    }

};

#endif // REGISTRAR_H
