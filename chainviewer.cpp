#include "chainviewer.h"

ChainViewer::ChainViewer(QWidget *parent,
                                 Rodbot::Vision::IPChain* ipChain, Rodbot::Vision::ChainRegistry *chainRegistry, UnitListModel *dialogRegistry, bool isVertical) :
    QScrollArea(parent), ipChain(ipChain), chainRegistry(chainRegistry), dialogRegistry(dialogRegistry)
{
    setAcceptDrops(true);

    if (isVertical) {
        setAlignment( Qt::AlignHCenter| Qt::AlignTop );
        scrollLayout = new QVBoxLayout;
        scrollWidgetLayout = new QVBoxLayout;
        //setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    } else {
        setAlignment( Qt::AlignVCenter| Qt::AlignLeft );
        scrollLayout = new QHBoxLayout;
        scrollWidgetLayout = new QHBoxLayout;
    }

    setLayout(scrollLayout);
    scrollWidget = new QWidget();
    scrollWidget->setLayout(scrollWidgetLayout);
    scrollWidgetLayout->addStretch();
    this->setWidget(scrollWidget);
    this->setWidgetResizable(true);

}

void ChainViewer::reloadViewerFromChain()
{
    // Update viewer if chain already has elements
    QVector<Rodbot::Vision::IPUnit*> unitsToRemove;
    if (ipChain->size() > 0) {
        Rodbot::Vision::IPChain::const_iterator it = ipChain->begin();
        bool showIgnoreMessage = false;
        for (; it != ipChain->end(); it++) {
            QString unitName = QString::fromStdString((*it)->name());
            if (dialogRegistry->hasDialog(unitName)) {
                QString dialogName = dialogRegistry->getDialogName(unitName);
                IPUnitContainer* newContainer = createUnitContainer(dialogName, unitName, it->get());
                addUnit(containerIndex.count()+1,newContainer);

            } else {
                showIgnoreMessage = true;
                unitsToRemove.push_back(it->get());
            }
        }

        foreach(Rodbot::Vision::IPUnit* unit, unitsToRemove) {
            ipChain->Remove(unit);
        }

        if (showIgnoreMessage)
            QMessageBox::warning(this,"Chain Load Warning", "Some of the units in the config file do not have dialogs and will be ignored.");
    }
}

void ChainViewer::mousePressEvent(QMouseEvent *event)
{
    // childAt only looks at the top visible widget
    // to check if there is a IPUnitContainer hidden underneath
    // we must loop through its parents
    QObject *child = childAt(event->pos());
    if (!child)
        return;

    IPUnitContainer *unitContainer = dynamic_cast<IPUnitContainer*>(child);
    if (!unitContainer) {
        bool hasIPUnitContainer = false;
        while (this != child->parent() ) {
            child = child->parent();
            unitContainer = dynamic_cast<IPUnitContainer*>(child);
            if (unitContainer) {
                hasIPUnitContainer = true;
                break;
            }
        }
        if (!hasIPUnitContainer)
            return;
    }


    // create human-readable unique label for unit container
    // for now dirty solution: the unit type followed by the pointer address to the container
    QString containerLabel = getLabel(unitContainer);
    // register in labels map
    containerDragDrogRegisty[containerLabel] = unitContainer;

    QByteArray itemData = containerLabel.toLatin1();

    QMimeData *mimeData = new QMimeData;
    mimeData->setData(IP_UNIT_CONTAINER_MIME_TYPE, itemData);

    QDrag *drag = new QDrag(this);
    drag->setMimeData(mimeData);

    // create drag pixmap
    QPixmap pixmap = QPixmap::grabWidget(unitContainer);
    pixmap = pixmap.scaledToWidth(0.6 * pixmap.width());
    QBrush b( QColor(0,0,0,128) );

    QPainter p(&pixmap);
    p.setBrush(b);
    p.setPen(Qt::NoPen);
    p.drawRect(0,0,pixmap.width(),pixmap.height());
    p.end();
    drag->setPixmap(pixmap);

    drag->setHotSpot(QPoint(drag->pixmap().width()/2,drag->pixmap().height()/2));
    unitContainer->hide();

    if (drag->exec() == Qt::MoveAction)
        unitContainer->show();
    else
        unitContainer->show();
}

void ChainViewer::dragEnterEvent(QDragEnterEvent *e)
{
    if(e->mimeData()->hasFormat(IP_UNIT_MIME_TYPE))
        e->acceptProposedAction();

    else if (e->mimeData()->hasFormat(IP_UNIT_CONTAINER_MIME_TYPE))
        e->acceptProposedAction();
}

void ChainViewer::dragMoveEvent(QDragMoveEvent *e)
{
    if(e->mimeData()->hasFormat(IP_UNIT_MIME_TYPE))
        e->acceptProposedAction();

    else if (e->mimeData()->hasFormat(IP_UNIT_CONTAINER_MIME_TYPE)){
        QString containerLabel(e->mimeData()->data(IP_UNIT_CONTAINER_MIME_TYPE));
        IPUnitContainer *draggedContainer = containerDragDrogRegisty.find(containerLabel).value();

        foreach(IPUnitContainer* container, containerIndex) {
            if (draggedContainer != container) {
                QRect dropRect(e->pos(),QSize(1,1));
                IPUnitContainer::InsertPositions insertPosition = container->atBoundary(dropRect);
                if (insertPosition == IPUnitContainer::TOP) {
                    qDebug() << getLabel(container) << "top" << e->answerRect() << e->pos();
                    container->drawInsertBar = IPUnitContainer::TOP;
                    container->update();
                    break;
                } else if (insertPosition == IPUnitContainer::BOTTOM) {
                    qDebug() << getLabel(container) << "bottom" << e->answerRect() << e->pos();
                    container->drawInsertBar = IPUnitContainer::BOTTOM;
                    container->update();
                    break;
                }
            }
        }
        e->acceptProposedAction();
    }
}

void ChainViewer::dropEvent(QDropEvent *event)
{
    if(event->mimeData()->hasFormat(IP_UNIT_MIME_TYPE)) {
        event->accept();

        QByteArray encodedData = event->mimeData()->data(IP_UNIT_MIME_TYPE);
        QDataStream stream(&encodedData, QIODevice::ReadOnly);

        QString unitName, dialogName;
        stream >> dialogName >> unitName;

        // If first unit append to front of chain
        // if not try to drag before/after an existing widget
        if (scrollWidgetLayout->count() < 2 ) {
            IPUnitContainer * newContainer = createUnitContainer(unitName, dialogName);
            //scrollWidgetLayout->insertWidget(0,newContainer);
            addUnit(1,newContainer);
            //printChainLayout();
        } else {

            QRect dropRect(event->pos(),QSize(1,1));

            foreach(IPUnitContainer* container, containerToChainUnit.keys()) {

                IPUnitContainer::InsertPositions insertPosition = container->atBoundary(dropRect);
                if (insertPosition == IPUnitContainer::TOP) {
                    IPUnitContainer * newContainer = createUnitContainer(unitName, dialogName);
                    qDebug() << getLabel(newContainer)  + " dropped before " << getLabel(container);
                    //scrollWidgetLayout->insertWidget(scrollWidgetLayout->indexOf(container),newContainer);
                    addUnit(containerIndex.indexOf(container),newContainer);
                    //printChainLayout();
                    event->accept();
                    break;
                } else if (insertPosition == IPUnitContainer::BOTTOM) {
                    IPUnitContainer * newContainer = createUnitContainer(unitName, dialogName);
                    qDebug() << getLabel(newContainer) + " dropped after " + getLabel(container);
                    addUnit(containerIndex.indexOf(container)+2,newContainer);
                    //printChainLayout();
                    event->accept();
                    break;
                }
            }
        }
    }

    else if (event->mimeData()->hasFormat(IP_UNIT_CONTAINER_MIME_TYPE)) {
        event->accept();
        QByteArray encodedData = event->mimeData()->data(IP_UNIT_CONTAINER_MIME_TYPE);
        QString containerLabel(encodedData);

        IPUnitContainer *droppedContainer = containerDragDrogRegisty.find(containerLabel).value();
        QRect dropRect(event->pos(),QSize(1,1));

        foreach(IPUnitContainer* container, containerIndex) {
            if (container != droppedContainer) {
                IPUnitContainer::InsertPositions insertPosition = container->atBoundary(dropRect);
                if (insertPosition == IPUnitContainer::TOP) {
                    qDebug() << containerLabel  + " dropped before " + getLabel(container) ;
                    moveUnit(containerIndex.indexOf(container),droppedContainer);
                    //printChainLayout();
                    event->accept();
                    break;
                } else if (insertPosition == IPUnitContainer::BOTTOM) {
                    qDebug() << containerLabel + " dropped after " + getLabel(container);
                    moveUnit(containerIndex.indexOf(container)+1,droppedContainer);
                    //printChainLayout();
                    event->accept();
                    break;
                }
            }
        }
    }
}

QString ChainViewer::getLabel(IPUnitContainer *container)
{
    // create human-readable unique label for unit container
    // for now dirty solution: the unit type followed by the pointer address to the container
    return container->unitName + "_" +  QString::number((size_t)container);
}

void ChainViewer::printChainLayout()
{
    for(int i=0; i < containerIndex.count(); i++) {
        IPUnitContainer* container = containerIndex[i];
        qDebug() << i << ": " << getLabel(container);
    }
}

int ChainViewer::indexOf(QLayoutItem *item) const
{
    for(int i = 0; i < scrollWidgetLayout->count(); i++) {
        if (scrollWidgetLayout->itemAt(i) == item) {
            return i;
        }
    }

    return -1;
}

void ChainViewer::addUnit(const int before, IPUnitContainer *unitContainer)
{
    containerIndex.insert(before,unitContainer);
    scrollWidgetLayout->insertWidget(before-1,unitContainer);

    setUpDownButtonVisibility();
}

IPUnitContainer* ChainViewer::createUnitContainer(const QString dialogName, const QString unitName, Rodbot::Vision::IPUnit *unit)
{
    // Invoke registry to create a IPUnit and associated dialog
    IPDialog* dialog = dialogRegistry->create(dialogName);

    if (unit == NULL) {
        unit = chainRegistry->IPUnitRegistrar.create(unitName.toStdString());
        ipChain->Append(unit);
    }

    IPUnitContainer* container = new IPUnitContainer(this,unit,dialog);

    dialog->updateUiFromSettings(unit->getSettings());

    containerToChainUnit[container] = unit;

    // connect remove signal
    connect(container,SIGNAL(removeUnitRequested()),
            SLOT(removeUnit()));
    // connect enabled signal
    connect(container,SIGNAL(enableChanged(bool)),SLOT(toggleEnableUnit(bool)));
    // connect changed settings signal
    connect(container,SIGNAL(settingsChanged()),SLOT(changeSettings()));
    // connect move up/down buttons
    connect(container,SIGNAL(moveRequested(bool)),SLOT(moveUnit(bool)));

    return container;

}

void ChainViewer::moveUnit(const int newIndex, IPUnitContainer *unitContainer)
{
    int prevIndex = containerIndex.indexOf(unitContainer);
    containerIndex.move(prevIndex,newIndex);

    ipChain->Move(newIndex,containerToChainUnit[unitContainer]);

    std::cout << *ipChain << std::endl;

    // handle layout
    scrollWidgetLayout->removeWidget(unitContainer);
    scrollWidgetLayout->insertWidget(newIndex,unitContainer);

    setUpDownButtonVisibility();
}

void ChainViewer::removeUnit(IPUnitContainer *unitContainer)
{
    // Delete dialog
    layout()->removeWidget(unitContainer);

    // Remove from chain
    Rodbot::Vision::IPUnit* chainUnit = containerToChainUnit[unitContainer];
    ipChain->Remove(chainUnit);

    qDebug() << getLabel(unitContainer) << " removed";

    // Deregister from map
    containerToChainUnit.remove(unitContainer);
    containerIndex.removeOne(unitContainer);
    delete unitContainer;
}

void ChainViewer::setUpDownButtonVisibility()
{
    foreach(IPUnitContainer* unitContainer, containerIndex) {
        int currentIndex = containerIndex.indexOf(unitContainer);
        unitContainer->setMoveButtonHidden(false,IPUnitContainer::UP_BUTTON |
                                           IPUnitContainer::DOWN_BUTTON);
        if (currentIndex == 0) {
            unitContainer->setMoveButtonHidden(true,IPUnitContainer::UP_BUTTON);
        }
        if (currentIndex == (containerIndex.size() - 1) ) {
            unitContainer->setMoveButtonHidden(true,IPUnitContainer::DOWN_BUTTON);
        }
    }
}

void ChainViewer::moveUnit(bool moveUp)
{
    IPUnitContainer* unitContainer = qobject_cast<IPUnitContainer*>(sender());
    int currentPos = containerIndex.indexOf(unitContainer);
    if (moveUp) {
        // move up by one if not already at front
        if (currentPos != 0)
             moveUnit(currentPos - 1,unitContainer);
    } else {
        // move down by one if not already at back
        if (currentPos < (containerIndex.size() - 1))
            moveUnit(currentPos + 1,unitContainer);
    }
    //printChainLayout();
}

void ChainViewer::removeUnit()
{

    IPUnitContainer* unitContainer = qobject_cast<IPUnitContainer*>(sender());

    removeUnit(unitContainer);
}

void ChainViewer::toggleEnableUnit(bool enable)
{
    IPUnitContainer* senderContainer = qobject_cast<IPUnitContainer*>(sender());

    Rodbot::Vision::IPUnit* unit = containerToChainUnit[senderContainer];
    unit->toggle(enable);

}

void ChainViewer::changeSettings()
{
    IPUnitContainer* unitContainer = qobject_cast<IPUnitContainer*>(sender());
    // first need to convert local version of settings to something
    // Rodbot::Vision chain can understand
    Rodbot::Vision::IPUnit* chainUnit = containerToChainUnit[unitContainer];
    Rodbot::Vision::UnitSettings settings = unitContainer->getSettings();
    chainUnit->applySettings(settings);

    qDebug() << getLabel(unitContainer) << " settings changed";
}

void ChainViewer::addUnit(QMimeData *mimeData)
{
    if (mimeData->hasFormat(IP_UNIT_MIME_TYPE)) {
        QByteArray encodedData = mimeData->data(IP_UNIT_MIME_TYPE);
        QDataStream stream(&encodedData, QIODevice::ReadOnly);

        QString unitName, dialogName;
        stream >> dialogName >> unitName;

        IPUnitContainer * newContainer = createUnitContainer(unitName,dialogName);
        //scrollWidgetLayout->insertWidget(scrollWidgetLayout->count()-1,newContainer);
        addUnit(containerIndex.count()+1,newContainer);
        qDebug() << getLabel(newContainer) << " added";
        //printChainLayout();

        delete mimeData;
    }
}

void ChainViewer::handleUnitError(Rodbot::Vision::IPUnit *unit)
{
    QString message = "An OpenCV error was encountered in one of the units.  It has been removed";
    QMessageBox::critical(0, "IPUnit OpenCV Error", message);

    for (QHash<IPUnitContainer*,Rodbot::Vision::IPUnit*>::iterator it = containerToChainUnit.begin();
         it != containerToChainUnit.end(); it++) {
        if (it.value() == unit) {
            removeUnit(it.key());
            // ok, problem should be fixed now
            waitForGoodChain.wakeAll();
            break;
        }
    }
}

void ChainViewer::loadChainConfig()
{
    // Open file dialog
    QString fileName = QFileDialog::getOpenFileName(this,tr("Open IPChain configuration"),"/Users/Sam/",tr("Config files (*.yaml);;All files (*)"));
   // QString fileName = "/home/samuelch/src/rodbotvision/examples/test.yaml";
    if (fileName.isEmpty())
        return;

    qDebug() << "opening " << fileName;

    ipChain->loadFromYaml(fileName.toStdString());

    reloadViewerFromChain();
}

void ChainViewer::saveChainConfig()
{
    // Open file dialog
    QString fileName = QFileDialog::getSaveFileName(this,tr("Save IPChain configuration"),"/Users/Sam/",tr("Config files (*.yaml);;All files (*)"));
    if (fileName.isEmpty())
        return;

    qDebug() << "saving to " << fileName;
    ipChain->saveToYaml(fileName.toStdString());
}
