#ifndef VIDEOVIEWER_H
#define VIDEOVIEWER_H

// Qt
#include <QWidget>
#include <QString>
#include <QMessageBox>
#include <QThread>
//OpenCV
#include <opencv2/highgui/highgui.hpp>
//IPChain
#include <IPChain/IPChain.hpp>
//local
//#include "IPChain.hpp"
#include "MatToQImage.h"
#include "Buffer.h"
#include "capture.h"
#include "processing.h"
#include "chainviewer.h"

namespace Ui {
class VideoViewer;
}

class VideoViewer : public QWidget
{
    Q_OBJECT

public:
    VideoViewer();
    explicit VideoViewer(QWidget *parent, Buffer<Mat>* buffer, Rodbot::Vision::IPChain* ipChain, ChainViewer* chainViewer);
    bool connectToFile(QString fileName, bool dropFrame, int width, int height);
    void closeFile();
    bool isfileConnected() const;
    ~VideoViewer();

private:
    Ui::VideoViewer *ui;
    Capture *capture;
    Processing* processing;
    QThread *captureThread;
    QThread *processingThread;
    QWaitCondition* waitForGoodChain;

    Buffer<Mat>* buffer;
    Rodbot::Vision::IPChain * ipChain;
    ChainViewer* chainViewer;
    cv::Mat currentFrame;
    bool fileConnected;

public slots:
    void clearImageBuffer();
    void stopCapture();
    void stopProcessing();

private slots:
    void updateFrame(const QImage &frame);
    void updateProcessingThreadStats(struct ThreadStatisticsData statData);
    void updateCaptureThreadStats(struct ThreadStatisticsData statData);
    void updateFps(int fps);

signals:
    void captureFinished();
    void processingFinished();
};

#endif // VIDEOVIEWER_H
