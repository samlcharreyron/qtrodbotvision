#include "unitlist.h"
#include <QDebug>
//Dialogs
#include "Dialogs/smoothingdialog.h"
#include "Dialogs/thresholddialog.h"
#include "Dialogs/adaptivethresholddialog.h"
#include "Dialogs/cannydialog.h"
#include "Dialogs/contoursdialog.h"
#include "Dialogs/morphologydialog.h"
#include "Dialogs/sobeldialog.h"
#include "Dialogs/laplaciandialog.h"
#include "Dialogs/blobdialog.h"

/********************
 * Model
 ********************/

UnitListModel::UnitListModel(QObject *parent) :
    QAbstractListModel(parent)
{
    reg("SmoothingDialog", new DialogClassDescriptor<SmoothingDialog>("BlurUnit"));
    reg("ThresholdDialog", new DialogClassDescriptor<ThresholdDialog>("ThresholdUnit"));
    reg("AdaptiveThresholdDialog", new DialogClassDescriptor<AdaptiveThresholdDialog>("AdaptiveThresholdUnit"));
    reg("CannyDialog", new DialogClassDescriptor<CannyDialog>("CannyUnit"));
    reg("ContoursDialog", new DialogClassDescriptor<ContoursDialog>("ContoursUnit"));
    reg("MorphologyDialog", new DialogClassDescriptor<MorphologyDialog>("MorphologyUnit"));
    reg("SobelDialog", new DialogClassDescriptor<SobelDialog>("SobelUnit"));
    reg("LaplacianDialog", new DialogClassDescriptor<LaplacianDialog>("LaplacianUnit"));
    reg("BlobDialog", new DialogClassDescriptor<BlobDialog>("BlobDetectorUnit"));
}

int UnitListModel::rowCount(const QModelIndex &) const
{
    return numRegistered();
}

QVariant UnitListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if (index.row() >= numRegistered())
        return QVariant();
    if (role == Qt::DisplayRole) {
        // return name string of unit
        return labelAt(index.row());
    } else
        return QVariant();
}

Qt::ItemFlags UnitListModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags defaultFlags = QAbstractListModel::flags(index);
    if (index.isValid())
        return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled |
                Qt::ItemIsDropEnabled | defaultFlags;
    else
        return defaultFlags;
}

bool UnitListModel::hasDialog(const QString &unitName) const
{
    QStringList unitNames = this->unitNames();
    return unitNames.contains(unitName);
}

const QString UnitListModel::getDialogName(const QString &unitName) const
{
    foreach( QString dialogName, classes.keys()) {
        if ( classes[dialogName]->unitName() == unitName) {
            return dialogName;
        }
    }

    return "";
}

QStringList UnitListModel::mimeTypes() const
{
    QStringList types;
    types << IP_UNIT_MIME_TYPE;
    types << "text/plain";
    return types;
}

QMimeData *UnitListModel::mimeData(const QModelIndexList &indexes) const
{
    QMimeData *mimeData = new QMimeData();
    QByteArray encodedData;

    QDataStream stream(&encodedData, QIODevice::WriteOnly);
    foreach (QModelIndex index, indexes) {
        if (index.isValid()) {
            stream << unitNameAt(index.row()) << nameAt(index.row());
        }
    }

    mimeData->setData(IP_UNIT_MIME_TYPE, encodedData);
    return mimeData;
}

void UnitListModel::handleDoubleClick(QModelIndex index)
{
    if(index.isValid()) {
        QMimeData* mimeData = new QMimeData();
        QByteArray encodedData;

        QDataStream stream(&encodedData, QIODevice::WriteOnly);

        stream << unitNameAt(index.row()) << nameAt(index.row());
        mimeData->setData(IP_UNIT_MIME_TYPE,encodedData);
        emit(unitDoubleClicked(mimeData));
    }
}

/********************
 * View
 ********************/

UnitListView::UnitListView(QWidget *parent, UnitListModel *model) :
    QListView(parent),
    unitListModel(model)
{
    setModel(model);
    setDragEnabled(true);
    setSelectionMode(QListView::SingleSelection);
    //setDropIndicatorShown(true);
    setDragDropMode(QListView::DragOnly);

    connect(this,SIGNAL(doubleClicked(QModelIndex)),model,SLOT(handleDoubleClick(QModelIndex)));
}

UnitListModel *UnitListView::getUnitListModel() const
{
    return unitListModel;
}

void UnitListView::dragEnterEvent(QDragEnterEvent *event)
{
   event->acceptProposedAction();
}

void UnitListView::dragMoveEvent(QDragMoveEvent *e)
{
    e->acceptProposedAction();
}

void UnitListView::dragLeaveEvent(QDragLeaveEvent *e)
{
    e->accept();
}
