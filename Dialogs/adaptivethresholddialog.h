#ifndef ADAPTIVETHRESHOLDDIALOG_H
#define ADAPTIVETHRESHOLDDIALOG_H

#include <QWidget>
#include <QMessageBox>

#include <IPChain/IPUnits/Threshold/Threshold.hpp>
//local
#include "ipdialoginterface.h"
#include "config.h"

namespace Ui {
class AdaptiveThresholdDialog;
}

class AdaptiveThresholdDialog : public IPDialog
{
    Q_OBJECT
    Q_INTERFACES(IPDialogInterface)

public:
    explicit AdaptiveThresholdDialog(QWidget *parent=0);
    virtual ~AdaptiveThresholdDialog();

    static const QString label();

private:
    Ui::AdaptiveThresholdDialog *ui;

    void validateDialogs();
    void updateUiFromSettings(const Rodbot::Vision::UnitSettings& unitSettings);

private slots:
    void updateSettingsFromDialogs();

signals:
    void newSettings();

};

#endif // ADAPTIVETHRESHOLDDIALOG_H
