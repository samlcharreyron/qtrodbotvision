#include "adaptivethresholddialog.h"
#include "ui_adaptivethresholddialog.h"

AdaptiveThresholdDialog::AdaptiveThresholdDialog(QWidget *parent) :
    IPDialog(parent),
    ui(new Ui::AdaptiveThresholdDialog)
{
    ui->setupUi(this);

    //updateUiFromSettings();

    // connect signals and slots
    connect(ui->methodComboBox,SIGNAL(currentIndexChanged(QString)),SLOT(updateSettingsFromDialogs()));
    connect(ui->typeComboBox,SIGNAL(currentIndexChanged(QString)),SLOT(updateSettingsFromDialogs()));
    connect(ui->maxValueEdit,SIGNAL(editingFinished()),SLOT(updateSettingsFromDialogs()));
    connect(ui->blockSizeValueEdit,SIGNAL(editingFinished()),SLOT(updateSettingsFromDialogs()));
    connect(ui->CValueEdit,SIGNAL(editingFinished()),SLOT(updateSettingsFromDialogs()));

}

AdaptiveThresholdDialog::~AdaptiveThresholdDialog()
{
    delete ui;
}

const QString AdaptiveThresholdDialog::label()
{
    return "Adaptive Threshold";
}

void AdaptiveThresholdDialog::updateSettingsFromDialogs()
{
    validateDialogs();

    if(ui->typeComboBox->currentText().compare("Binary") == 0)
    {
        settings["type"] = Rodbot::Vision::ThresholdTypes::BINARY;
    } else if (ui->typeComboBox->currentText().compare("Binary Inverted") == 0)
    {
        settings["type"] = Rodbot::Vision::ThresholdTypes::BINARY_INV;
    }

    if (ui->methodComboBox->currentIndex() == 0)
        settings["method"] = cv::ADAPTIVE_THRESH_MEAN_C;
    else
        settings["method"] = cv::ADAPTIVE_THRESH_GAUSSIAN_C;

    settings["maxValue"] = ui->maxValueEdit->text().toDouble();
    settings["blockSize"] = ui->blockSizeValueEdit->text().toInt();
    settings["C"] = ui->CValueEdit->text().toDouble();

    emit newSettings();

}

void AdaptiveThresholdDialog::validateDialogs()
{
    bool inputEmpty = false;
    // check for empty inputs: if empty, set to default values

    if (ui->maxValueEdit->text().isEmpty())
    {
        ui->maxValueEdit->setText(QString::number(DEFAULT_THRESH_MAX_VAL));
        inputEmpty = true;
    }

    if (ui->blockSizeValueEdit->text().isEmpty())
    {
        ui->blockSizeValueEdit->setText(QString::number(DEFAULT_ADAPT_THRESH_BLOCK));
        inputEmpty = true;
    }

    if (ui->CValueEdit->text().isEmpty())
    {
        ui->CValueEdit->setText(QString::number(DEFAULT_ADAPT_THRESH_C));
        inputEmpty = true;
    }

    // check bounds
    if (ui->maxValueEdit->text().toDouble() > 255.0) {
        ui->maxValueEdit->setText(QString::number(255.0));
        QMessageBox::information(this->parentWidget(),"NOTE:","Max value must be between 0 and 255 \n\nAutomatically set to 255.");
    }

    if (ui->maxValueEdit->text().toDouble() < 0.0) {
        ui->maxValueEdit->setText(QString::number(0.0));
        QMessageBox::information(this->parentWidget(),"NOTE:","Max value must be between 0 and 255 \n\nAutomatically set to 0.");
    }

    if (ui->blockSizeValueEdit->text().toInt()%2==0) {
        ui->blockSizeValueEdit->setText(QString::number(ui->blockSizeValueEdit->text().toInt() + 1));
        QMessageBox::information(this->parentWidget(),"NOTE:","Block size must be ODD \n\nAutomatically set to size+1");
    }

    if (ui->blockSizeValueEdit->text().toInt() < 3) {
        ui->blockSizeValueEdit->setText(QString::number(DEFAULT_ADAPT_THRESH_BLOCK));
        QMessageBox::information(this->parentWidget(),"NOTE:","Block size should be >3 \n\nAutomatically set to default");
    }

    if (inputEmpty)
        QMessageBox::warning(this->parentWidget(),"WARNING:","One or more inputs empty.\n\nAutomatically set to default values.");

}

void AdaptiveThresholdDialog::updateUiFromSettings(const Rodbot::Vision::UnitSettings &unitSettings)
{
    Rodbot::Vision::AdaptiveThresholdMethods::T method = static_cast<Rodbot::Vision::AdaptiveThresholdMethods::T>(SETTING_TO_INT(unitSettings,type));
    switch(method) {
    case Rodbot::Vision::AdaptiveThresholdMethods::MEAN:
    {
        ui->methodComboBox->setCurrentIndex(0);
        break;
    }
    case Rodbot::Vision::AdaptiveThresholdMethods::GAUSSIAN:
    {
        ui->methodComboBox->setCurrentIndex(1);
    }
    }

    Rodbot::Vision::ThresholdTypes::T type = static_cast<Rodbot::Vision::ThresholdTypes::T>(SETTING_TO_INT(unitSettings,type));
    switch(type) {
    case Rodbot::Vision::ThresholdTypes::BINARY:
    {
        ui->typeComboBox->setCurrentIndex(0);
        break;
    }
    case Rodbot::Vision::ThresholdTypes::BINARY_INV:
    {
        ui->typeComboBox->setCurrentIndex(1);
        break;
    }
    }

    ui->maxValueEdit->setText(SETTING_TO_QSTRING(unitSettings,maxValue));
    ui->blockSizeValueEdit->setText(SETTING_TO_QSTRING(unitSettings,blockSize));
    ui->CValueEdit->setText(SETTING_TO_QSTRING(unitSettings,C));
}
