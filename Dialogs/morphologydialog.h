#ifndef MORPHOLOGYDIALOG_H
#define MORPHOLOGYDIALOG_H

#include <QWidget>
#include <QMessageBox>
// external
#include <IPChain/IPUnits/Morphology/Morphology.hpp>
#include <opencv2/imgproc/imgproc.hpp>
//local
#include "ipdialoginterface.h"
#include "units.h"

namespace Ui {
class MorphologyDialog;
}

class MorphologyDialog : public IPDialog
{
    Q_OBJECT
    Q_INTERFACES(IPDialogInterface)

public:
    explicit MorphologyDialog(QWidget *parent = 0);
    virtual ~MorphologyDialog();

    virtual void resetDefaults();

    static const QString label();

private:
    Ui::MorphologyDialog *ui;
    void validateDialogs();
    void updateUiFromSettings(const Rodbot::Vision::UnitSettings &unitSettings);

private slots:
    void updateSettingsFromDialogs();

signals:
    void newSettings();
};

#endif // MORPHOLOGYDIALOG_H
