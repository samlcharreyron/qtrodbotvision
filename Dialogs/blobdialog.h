#ifndef BLOBDIALOG_H
#define BLOBDIALOG_H

#include <QWidget>
#include "ipdialoginterface.h"

namespace Ui {
class BlobDialog;
}

class BlobDialog : public IPDialog
{
    Q_OBJECT
    Q_INTERFACES(IPDialogInterface)
    
public:
    explicit BlobDialog(QWidget *parent = 0);
    ~BlobDialog();

    static const QString label();
    
private:
    Ui::BlobDialog *ui;

    void validateDialogs();
    void updateUiFromSettings(const Rodbot::Vision::UnitSettings &unitSettings);

private slots:
    void updateSettingsFromDialogs();

signals:
    void newSettings();
};

#endif // BLOBDIALOG_H
