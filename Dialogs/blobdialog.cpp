#include "blobdialog.h"
#include "ui_blobdialog.h"
#include <boost/lexical_cast.hpp>

BlobDialog::BlobDialog(QWidget *parent) :
    IPDialog(parent),
    ui(new Ui::BlobDialog)
{
    ui->setupUi(this);

    connect(ui->maxThreshBox, SIGNAL(editingFinished()), SLOT(updateSettingsFromDialogs()));
    connect(ui->minThreshBox, SIGNAL(editingFinished()), SLOT(updateSettingsFromDialogs()));
    connect(ui->threshStepBox, SIGNAL(editingFinished()), SLOT(updateSettingsFromDialogs()));
    connect(ui->minAreaBox, SIGNAL(editingFinished()), SLOT(updateSettingsFromDialogs()));
    connect(ui->maxAreaBox, SIGNAL(editingFinished()), SLOT(updateSettingsFromDialogs()));
    connect(ui->whiteButton, SIGNAL(toggled(bool)), SLOT(updateSettingsFromDialogs()));
    connect(ui->blackButton, SIGNAL(toggled(bool)), SLOT(updateSettingsFromDialogs()));
}

BlobDialog::~BlobDialog()
{
    delete ui;
}

const QString BlobDialog::label()
{
    return "Blob Detection";
}

void BlobDialog::validateDialogs()
{
    if (ui->minThreshBox->value() > ui->maxThreshBox->value())
        ui->minThreshBox->setValue(1);
    if (ui->minAreaBox->value() > ui->maxAreaBox->value())
        ui->minAreaBox->setValue(1.0);
    if (ui->threshStepBox->value() > (ui->maxThreshBox->value() - ui->minThreshBox->value()))
        ui->threshStepBox->setValue(1);
}

void BlobDialog::updateUiFromSettings(const Rodbot::Vision::UnitSettings &unitSettings)
{
    ui->minThreshBox->setValue(SETTING_TO_INT(unitSettings,minThreshold));
    ui->maxThreshBox->setValue(SETTING_TO_INT(unitSettings,maxThreshold));
    ui->threshStepBox->setValue(SETTING_TO_INT(unitSettings,thresholdStep));
    ui->minAreaBox->setValue(SETTING_TO_DOUBLE(unitSettings,minArea));
    ui->maxAreaBox->setValue(SETTING_TO_DOUBLE(unitSettings,maxArea));
    if (unitSettings.find("blobColor")->second == "0")
        ui->blackButton->toggle();
    else
        ui->whiteButton->toggle();
}

void BlobDialog::updateSettingsFromDialogs()
{
    validateDialogs();

    settings["minThreshold"] = ui->minThreshBox->value();
    settings["maxThreshold"] = ui->maxThreshBox->value();
    settings["thresholdStep"] = ui->threshStepBox->value();
    settings["minArea"] = ui->minAreaBox->value();
    settings["maxArea"] = ui->maxAreaBox->value();
    settings["filterByColor"] = "1";
    settings["filterByArea"] = "1";
    if (ui->blackButton->isChecked())
        settings["blobColor"] = "0";
    else
        settings["blobColor"] = "255";

    emit newSettings();
}
