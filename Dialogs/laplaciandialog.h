#ifndef LAPLACIANDIALOG_H
#define LAPLACIANDIALOG_H

#include <QWidget>
//local
#include "ipdialoginterface.h"

namespace Ui {
class LaplacianDialog;
}

class LaplacianDialog : public IPDialog
{
    Q_OBJECT
    Q_INTERFACES(IPDialogInterface)

public:
    explicit LaplacianDialog(QWidget *parent = 0);
    ~LaplacianDialog();

    static const QString label();

private:
    Ui::LaplacianDialog *ui;
    void validateDialogs();
    void updateUiFromSettings(const Rodbot::Vision::UnitSettings& unitSettings);

private slots:
    void updateSettingsFromDialogs();

signals:
    void newSettings();
};

#endif // LAPLACIANDIALOG_H
