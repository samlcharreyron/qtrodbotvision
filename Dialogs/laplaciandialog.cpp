#include "laplaciandialog.h"
#include "ui_laplaciandialog.h"

LaplacianDialog::LaplacianDialog(QWidget *parent) :
    IPDialog(parent),
    ui(new Ui::LaplacianDialog)
{
    ui->setupUi(this);

    connect(ui->kSizeBox, SIGNAL(valueChanged(int)), SLOT(updateSettingsFromDialogs()));
}

LaplacianDialog::~LaplacianDialog()
{
    delete ui;
}

const QString LaplacianDialog::label()
{
    return "Laplacian Filter";
}

void LaplacianDialog::validateDialogs()
{
    if((ui->kSizeBox->value() %2) == 0) {
        ui->kSizeBox->setValue(ui->kSizeBox->value()+1);
    }
}

void LaplacianDialog::updateUiFromSettings(const Rodbot::Vision::UnitSettings &unitSettings)
{
    ui->kSizeBox->setValue(SETTING_TO_INT(unitSettings,ksize));
}

void LaplacianDialog::updateSettingsFromDialogs()
{
    validateDialogs();

    settings["ksize"] = ui->kSizeBox->value();

    emit(newSettings());
}
