#include "cannydialog.h"
#include "ui_cannydialog.h"

CannyDialog::CannyDialog(QWidget *parent) :
    IPDialog(parent),
    ui(new Ui::CannyDialog)
{
    ui->setupUi(this);

    //connect signals and slots
    connect(ui->thresh1ValueEdit,SIGNAL(editingFinished()),SLOT(updateSettingsFromDialogs()));
    connect(ui->thresh2ValueEdit,SIGNAL(editingFinished()),SLOT(updateSettingsFromDialogs()));
    connect(ui->apertureValueEdit,SIGNAL(editingFinished()),SLOT(updateSettingsFromDialogs()));
    connect(ui->l2CheckBox,SIGNAL(released()),SLOT(updateSettingsFromDialogs()));

}

CannyDialog::~CannyDialog()
{
    delete ui;
}

const QString CannyDialog::label()
{
    return "Canny Filter";
}

void CannyDialog::validateDialogs()
{
    int apSize = ui->apertureValueEdit->text().toInt();
    if (apSize != 1 && apSize != 3 && apSize != 5 && apSize != 7) {
        ui->apertureValueEdit->setText(QString::number(DEFAULT_CANNY_APERTURE));
        QMessageBox::information(this->parentWidget(),"NOTE:","Aperture size must be 1,3,5 or 7\n\nAutomatically set to default");
    }

    if (ui->thresh1ValueEdit->text().toDouble() > 255.0 ||
            ui->thresh1ValueEdit->text().toDouble() < 0) {
        ui->thresh1ValueEdit->setText(QString::number(DEFAULT_CANNY_THRESH1));
        QMessageBox::information(this->parentWidget(),"NOTE:","Threshold must be between 0 and 255\n\nAutomatically set to default");
    }

    if (ui->thresh2ValueEdit->text().toDouble() > 255.0 ||
            ui->thresh2ValueEdit->text().toDouble() < 0) {
        ui->thresh2ValueEdit->setText(QString::number(DEFAULT_CANNY_THRESH2));
        QMessageBox::information(this->parentWidget(),"NOTE:","Threshold must be between 0 and 255\n\nAutomatically set to default");
    }

    // check for empty inputs
    bool inputEmpty = false;
    if(ui->apertureValueEdit->text().isEmpty())
    {
        ui->apertureValueEdit->setText(QString::number(DEFAULT_CANNY_APERTURE));
        inputEmpty=true;
    }
    if(ui->thresh1ValueEdit->text().isEmpty())
    {
        ui->thresh1ValueEdit->setText(QString::number(DEFAULT_CANNY_THRESH1));
        inputEmpty=true;
    }
    if(ui->thresh2ValueEdit->text().isEmpty())
    {
        ui->thresh2ValueEdit->setText(QString::number(DEFAULT_CANNY_THRESH2));
        inputEmpty=true;
    }

    if(inputEmpty)
        QMessageBox::warning(this->parentWidget(),"WARNING:","One or more inputs empty.\n\nAutomatically set to default values.");
}

void CannyDialog::updateUiFromSettings(const Rodbot::Vision::UnitSettings &unitSettings)
{
    ui->thresh1ValueEdit->setText(SETTING_TO_QSTRING(unitSettings,thresh1));
    ui->apertureValueEdit->setText(SETTING_TO_QSTRING(unitSettings,apertureSize));

    if (unitSettings.find("L2Gradient")->second.compare("1") == 0)
        ui->l2CheckBox->setChecked(true);
    else
        ui->l2CheckBox->setChecked(false);
}

void CannyDialog::updateSettingsFromDialogs()
{
    validateDialogs();

    settings["thresh1"] = ui->thresh1ValueEdit->text().toInt();
    settings["thresh2"] = ui->thresh2ValueEdit->text().toInt();
    settings["apertureSize"] = ui->apertureValueEdit->text().toInt();
    settings["L2Gradient"] = ui->l2CheckBox->isChecked();

    emit newSettings();

}
