#include "sobeldialog.h"
#include "ui_sobeldialog.h"

SobelDialog::SobelDialog(QWidget *parent) :
    IPDialog(parent),
    ui(new Ui::SobelDialog)
{
    ui->setupUi(this);

    // connect signals and slots
    connect(ui->dxBox,SIGNAL(valueChanged(int)),SLOT(updateSettingsFromDialogs()));
    connect(ui->dyBox,SIGNAL(valueChanged(int)),SLOT(updateSettingsFromDialogs()));
    connect(ui->ksizeBox,SIGNAL(valueChanged(int)),SLOT(updateSettingsFromDialogs()));
    //connect(ui->overlayBox,SIGNAL(toggled(bool),SLOT())
}

SobelDialog::~SobelDialog()
{
    delete ui;
}

const QString SobelDialog::label()
{
    return "Sobel Filter";
}

void SobelDialog::validateDialogs()
{
    if ((ui->ksizeBox->value() % 2) == 0) {
        ui->ksizeBox->setValue(ui->ksizeBox->value()+1);
        QMessageBox::warning(this->parentWidget(),"ERROR:","kernel size must be odd.\n\nAutomatically incremented to next odd size.");
    }

    if (ui->ksizeBox->value() == 1 &&
            (ui->dxBox->value() > 2)) {
        ui->dxBox->setValue(2);
    }

    if (ui->ksizeBox->value() == 1 &&
            (ui->dyBox->value() > 2)) {
        ui->dyBox->setValue(2);
    }
}

void SobelDialog::updateUiFromSettings(const Rodbot::Vision::UnitSettings &unitSettings)
{
    ui->dxBox->setValue(SETTING_TO_INT(unitSettings, xorder));
    ui->dyBox->setValue(SETTING_TO_INT(unitSettings, yorder));
    ui->ksizeBox->setValue(SETTING_TO_INT(unitSettings, ksize));
}

void SobelDialog::updateSettingsFromDialogs()
{
    validateDialogs();

    settings["xorder"] = ui->dxBox->value();
    settings["yorder"] = ui->dyBox->value();
    settings["ksize"] =  ui->ksizeBox->value();
}

void SobelDialog::setOverlay(bool toOriginal)
{
    
}
