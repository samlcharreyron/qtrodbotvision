#ifndef CANNYDIALOG_H
#define CANNYDIALOG_H

#include <QWidget>
#include <QMessageBox>
//local
#include "ipdialoginterface.h"
#include "units.h"

namespace Ui {
class CannyDialog;
}

class CannyDialog : public IPDialog
{
    Q_OBJECT
    Q_INTERFACES(IPDialogInterface)

public:
    explicit CannyDialog(QWidget *parent = 0);
    ~CannyDialog();

    static const QString label();

private:
    Ui::CannyDialog *ui;
    void validateDialogs();
    void updateUiFromSettings(const Rodbot::Vision::UnitSettings &unitSettings);

private slots:
    void updateSettingsFromDialogs();

signals:
    void newSettings();
};

#endif // CANNYDIALOG_H
