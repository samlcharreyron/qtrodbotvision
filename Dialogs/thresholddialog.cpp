#include "thresholddialog.h"
#include "ui_thresholddialog.h"

ThresholdDialog::ThresholdDialog(QWidget *parent) :
    IPDialog(parent),
    ui(new Ui::ThresholdDialog)
{
    ui->setupUi(this);

    // connect signals and slots
    connect(ui->typeComboBox,SIGNAL(currentIndexChanged(QString)),SLOT(updateSettingsFromDialogs()));
    connect(ui->valueEdit,SIGNAL(editingFinished()),SLOT(updateSettingsFromDialogs()));
    connect(ui->maxValueEdit,SIGNAL(editingFinished()),SLOT(updateSettingsFromDialogs()));

}

ThresholdDialog::~ThresholdDialog()
{
    delete ui;
}

const QString ThresholdDialog::label()
{
    return "Threshold";
}

void ThresholdDialog::updateSettingsFromDialogs()
{
    validateDialogs();

    if(ui->typeComboBox->currentText().compare("Binary") == 0)
    {
        settings["type"] = Rodbot::Vision::ThresholdTypes::BINARY;
    } else if (ui->typeComboBox->currentText().compare("Binary Inverted") == 0)
    {
        settings["type"] = Rodbot::Vision::ThresholdTypes::BINARY_INV;
    }
    else if (ui->typeComboBox->currentText().compare("Truncated") == 0)
    {
        settings["type"] = Rodbot::Vision::ThresholdTypes::TRUNC;
    }
    else if (ui->typeComboBox->currentText().compare("To Zero") == 0)
    {
        settings["type"] = Rodbot::Vision::ThresholdTypes::TO_ZERO;
    } else
    {
        settings["type"] = Rodbot::Vision::ThresholdTypes::TO_ZERO_INV;
    }

    settings["value"] = ui->valueEdit->text().toDouble();
    settings["maxValue"] = ui->valueEdit->text().toDouble();

    emit newSettings();

}

void ThresholdDialog::validateDialogs()
{
    bool inputEmpty = false;
    // check for empty inputs: if empty, set to default values
    if (ui->valueEdit->text().isEmpty())
    {
        ui->valueEdit->setText(QString::number(DEFAULT_THRESH_VAL));
        inputEmpty = true;
    }

    if (ui->maxValueEdit->text().isEmpty())
    {
        ui->maxValueEdit->setText(QString::number(DEFAULT_THRESH_MAX_VAL));
        inputEmpty = true;
    }

    // check bounds
    if (ui->valueEdit->text().toDouble() > 255.0) {
        ui->valueEdit->setText(QString::number(255.0));
        QMessageBox::information(this->parentWidget(),"NOTE:","Value must be between 0 and 255 \n\nAutomatically set to 255.");
    }

    if (ui->valueEdit->text().toDouble() < 0.0) {
        ui->valueEdit->setText(QString::number(0.0));
        QMessageBox::information(this->parentWidget(),"NOTE:","Value must be between 0 and 255 \n\nAutomatically set to 0.");
    }

    if (inputEmpty)
        QMessageBox::warning(this->parentWidget(),"WARNING:","One or more inputs empty.\n\nAutomatically set to default values.");

}

void ThresholdDialog::updateUiFromSettings(const Rodbot::Vision::UnitSettings& unitSettings)
{
    Rodbot::Vision::ThresholdTypes::T type = static_cast<Rodbot::Vision::ThresholdTypes::T>(SETTING_TO_INT(unitSettings,type));
    switch(type) {
    case Rodbot::Vision::ThresholdTypes::BINARY:
    {
        ui->typeComboBox->setCurrentIndex(0);
        break;
    }
    case Rodbot::Vision::ThresholdTypes::BINARY_INV:
    {
        ui->typeComboBox->setCurrentIndex(1);
        break;
    }
    case Rodbot::Vision::ThresholdTypes::TRUNC:
    {
        ui->typeComboBox->setCurrentIndex(2);
        break;
    }
    case Rodbot::Vision::ThresholdTypes::TO_ZERO:
    {
        ui->typeComboBox->setCurrentIndex(3);
        break;
    }
    case Rodbot::Vision::ThresholdTypes::TO_ZERO_INV:
    {
        ui->typeComboBox->setCurrentIndex(4);
        break;
    }
    }

    ui->valueEdit->setText(SETTING_TO_QSTRING(unitSettings,value));
    ui->maxValueEdit->setText(SETTING_TO_QSTRING(unitSettings,maxValue));
}
