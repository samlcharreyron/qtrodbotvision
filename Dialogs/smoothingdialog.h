#ifndef SMOOTHINGDIALOG_H
#define SMOOTHINGDIALOG_H

#include <QWidget>
#include <QMessageBox>
//local
#include "ipdialoginterface.h"
#include "units.h"

namespace Ui {
class SmoothingDialog;
}

class SmoothingDialog : public IPDialog
{
    Q_OBJECT
    Q_INTERFACES(IPDialogInterface)

    typedef Rodbot::Vision::SmoothingTypes::T SmoothingType;

public:
    explicit SmoothingDialog(QWidget* parent=0);
    virtual ~SmoothingDialog();

    // how to label this dialog in the unit list
    static const QString label();

private:
    Ui::SmoothingDialog *ui;
    void validateDialogs();
    void updateUiFromSettings(const Rodbot::Vision::UnitSettings &unitSettings);

private slots:
    void updateSettingsFromDialogs();

signals:
    void newSettings();

};

#endif // SMOOTHINGUNIT_H
