#include "contoursdialog.h"
#include "ui_contoursdialog.h"

ContoursDialog::ContoursDialog(QWidget *parent) :
    IPDialog(parent),
    ui(new Ui::ContoursDialog)
{
    ui->setupUi(this);

    // connect signals and slots
    // edits
    connect(ui->areaThreshEdit,SIGNAL(editingFinished()),SLOT(updateSettingsFromDialogs()));
    connect(ui->epsAreaEdit,SIGNAL(editingFinished()),SLOT(updateSettingsFromDialogs()));
    connect(ui->offsetXEdit,SIGNAL(editingFinished()),SLOT(updateSettingsFromDialogs()));
    connect(ui->offsetYEdit,SIGNAL(editingFinished()),SLOT(updateSettingsFromDialogs()));
    // spinbox
    connect(ui->maxLevelSpinBox,SIGNAL(valueChanged(int)),SLOT(updateSettingsFromDialogs()));
    connect(ui->thicknessSpinBox,SIGNAL(valueChanged(int)),SLOT(updateSettingsFromDialogs()));
    // checkbox
    connect(ui->eightsideCheckbox,SIGNAL(released()),SLOT(updateSettingsFromDialogs()));
    connect(ui->trianglesCheckbox,SIGNAL(released()),SLOT(updateSettingsFromDialogs()));
    connect(ui->rectanglesCheckbox,SIGNAL(released()),SLOT(updateSettingsFromDialogs()));
    connect(ui->lshapeCheckbox,SIGNAL(released()),SLOT(updateSettingsFromDialogs()));
    connect(ui->ignoreInsideCheckbox,SIGNAL(released()),SLOT(updateSettingsFromDialogs()));
    // combobox
    connect(ui->lineTypeComboBox,SIGNAL(currentIndexChanged(int)),SLOT(updateSettingsFromDialogs()));
    connect(ui->modeComboBox,SIGNAL(currentIndexChanged(int)),SLOT(updateSettingsFromDialogs()));
    connect(ui->methodComboBox,SIGNAL(currentIndexChanged(int)),SLOT(updateSettingsFromDialogs()));
}

ContoursDialog::~ContoursDialog()
{
    delete ui;
}

const QString ContoursDialog::label()
{
    return "Contour Detection";
}

void ContoursDialog::validateDialogs()
{
    // check for empty inputs
    bool inputEmpty = false;

    if(ui->areaThreshEdit->text().isEmpty())
    {
        ui->areaThreshEdit->setText(QString::number(DEFAULT_CONTOURS_AREA_THRESH));
        inputEmpty=true;
    }
    if(ui->epsAreaEdit->text().isEmpty())
    {
        ui->epsAreaEdit->setText(QString::number(DEFAULT_CONTOURS_EPS_AREA_SF));
        inputEmpty=true;
    }
    if(ui->offsetXEdit->text().isEmpty())
    {
        ui->offsetXEdit->setText(QString::number(DEFAULT_CONTOURS_OFFSET_X));
        inputEmpty=true;
    }
    if(ui->offsetYEdit->text().isEmpty())
    {
        ui->offsetYEdit->setText(QString::number(DEFAULT_CONTOURS_OFFSET_Y));
        inputEmpty=true;
    }
    if(inputEmpty)
        QMessageBox::warning(this->parentWidget(),"WARNING:","One or more inputs empty.\n\nAutomatically set to default values.");

}

void ContoursDialog::updateUiFromSettings(const Rodbot::Vision::UnitSettings &unitSettings)
{
    // checkboxes
    ui->ignoreInsideCheckbox->setChecked(unitSettings.find("ignoreInside")->second.compare("1"));
    int shapeOptions = SETTING_TO_INT(unitSettings, shapeOptions);
    ui->trianglesCheckbox->setChecked((shapeOptions & Rodbot::Vision::TRIANGLES) != 0);
    ui->rectanglesCheckbox->setChecked((shapeOptions & Rodbot::Vision::RECTANGLES) != 0);
    ui->lshapeCheckbox->setChecked((shapeOptions & Rodbot::Vision::LSHAPE) != 0);
    ui->eightsideCheckbox->setChecked((shapeOptions & Rodbot::Vision::EIGHTSIDES) != 0);
    // spin boxes
    ui->thicknessSpinBox->setValue(SETTING_TO_INT(unitSettings, thickness));
    ui->maxLevelSpinBox->setValue(SETTING_TO_INT(unitSettings, maxLevel));
    // combo boxes
    ui->modeComboBox->setCurrentIndex(SETTING_TO_INT(unitSettings, mode));
    ui->methodComboBox->setCurrentIndex(SETTING_TO_INT(unitSettings, method)-1);
    int lineType = SETTING_TO_INT(unitSettings, lineType);
    switch(lineType) {
    case 8: {
        ui->lineTypeComboBox->setCurrentIndex(0);
        break;
    }
    case 4: {
        ui->lineTypeComboBox->setCurrentIndex(1);
        break;
    }
    case CV_AA: {
        ui->lineTypeComboBox->setCurrentIndex(2);
        break;
    }
    }

    // edits
    ui->areaThreshEdit->setText(SETTING_TO_QSTRING(unitSettings, areaThresh));
    ui->epsAreaEdit->setText(SETTING_TO_QSTRING(unitSettings, epsAreaSf));
    ui->offsetXEdit->setText(SETTING_TO_QSTRING(unitSettings, offsetX));
    ui->offsetYEdit->setText(SETTING_TO_QSTRING(unitSettings, offsetY));
}

void ContoursDialog::updateSettingsFromDialogs()
{
    validateDialogs();

    // checkboxes
    settings["ignoreInside"] = ui->ignoreInsideCheckbox->isChecked();
    int shapeOptions = 0;
    if (ui->trianglesCheckbox->isChecked()) {
        shapeOptions |= Rodbot::Vision::TRIANGLES;
    }
    if (ui->rectanglesCheckbox->isChecked()) {
        shapeOptions |= Rodbot::Vision::RECTANGLES;
    }
    if (ui->eightsideCheckbox->isChecked()) {
        shapeOptions |= Rodbot::Vision::EIGHTSIDES;
    }
    if (ui->lshapeCheckbox->isChecked()) {
        shapeOptions |= Rodbot::Vision::LSHAPE;
    }
    settings["shapeOptions"] = shapeOptions;
    // spin boxes
    settings["thickness"] = ui->thicknessSpinBox->value();
    settings["maxLevel"] = ui->maxLevelSpinBox->value();
    // combo boxes
    settings["mode"] = ui->modeComboBox->currentIndex();
    settings["method"] = ui->methodComboBox->currentIndex() + 1;
    int lineTypeIndex = ui->methodComboBox->currentIndex();
    switch(lineTypeIndex) {
    case 0: {
        settings["lineType"] = 8;
        break;
    }
    case 1: {
        settings["lineType"] = 4;
        break;
    }
    case 2: {
        settings["lineType"] = CV_AA;
        break;
    }
    }
    // edits
    settings["areaThresh"] = ui->areaThreshEdit->text().toDouble();
    settings["epsAreaSf"] = ui->epsAreaEdit->text().toDouble();
    settings["offsetX"] = ui->offsetXEdit->text().toInt();
    settings["offsetY"] = ui->offsetYEdit->text().toInt();

    emit newSettings();

}
