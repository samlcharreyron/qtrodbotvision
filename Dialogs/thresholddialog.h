#ifndef THRESHOLDDIALOG_H
#define THRESHOLDDIALOG_H

#include <QWidget>
#include <QMessageBox>
#include <IPChain/IPUnits/Threshold/Threshold.hpp>
//local
#include "ipdialoginterface.h"
#include "config.h"

namespace Ui {
class ThresholdDialog;
}

class ThresholdDialog : public IPDialog
{
    Q_OBJECT
    Q_INTERFACES(IPDialogInterface)

public:
    explicit ThresholdDialog(QWidget *parent = 0);
    virtual ~ThresholdDialog();

    static const QString label();

private slots:
    void updateSettingsFromDialogs();

private:
    Ui::ThresholdDialog *ui;
    Rodbot::Vision::IPUnit * unit;

    void validateDialogs();
    void updateUiFromSettings(const Rodbot::Vision::UnitSettings &unitSettings);

signals:
    void newSettings();

};

#endif // THRESHOLDDIALOG_H
