#include "morphologydialog.h"
#include "ui_morphologydialog.h"

MorphologyDialog::MorphologyDialog(QWidget *parent) :
    IPDialog(parent),
    ui(new Ui::MorphologyDialog)
{
    ui->setupUi(this);

    // connect signals and slots
    connect(ui->erosionButton,SIGNAL(released()),SLOT(updateSettingsFromDialogs()));
    connect(ui->dilationButton,SIGNAL(released()),SLOT(updateSettingsFromDialogs()));
    connect(ui->kernelSizeBox,SIGNAL(valueChanged(int)),SLOT(updateSettingsFromDialogs()));
    connect(ui->anchorXBox,SIGNAL(valueChanged(int)),SLOT(updateSettingsFromDialogs()));
    connect(ui->anchorYBox,SIGNAL(valueChanged(int)),SLOT(updateSettingsFromDialogs()));
    connect(ui->kernelTypeBox,SIGNAL(currentIndexChanged(int)),SLOT(updateSettingsFromDialogs()));
}

MorphologyDialog::~MorphologyDialog()
{
    delete ui;
}

void MorphologyDialog::resetDefaults()
{
//    unit->resetDefault();
//    updateUiFromSettings();
}

const QString MorphologyDialog::label()
{
    return "Morphology";
}

void MorphologyDialog::validateDialogs()
{

    if ((ui->kernelSizeBox->value() %2) == 0  ) {
        ui->kernelSizeBox->setValue(ui->kernelSizeBox->value()+1);
        //QMessageBox::information(this->parentWidget(),"NOTE:","Size 1 must be an ODD number.\n\nAutomatically set to (inputted value+1).");
    }

    if (ui->anchorXBox->value() > ui->kernelSizeBox->value()) {
        ui->anchorXBox->setValue(-1);
        QMessageBox::information(this->parentWidget(),"NOTE:","Anchor position must be inside the kernel.\n\nAutomatically set to center.");
    }
    if (ui->anchorYBox->value() > ui->kernelSizeBox->value()) {
        ui->anchorYBox->setValue(-1);
        QMessageBox::information(this->parentWidget(),"NOTE:","Anchor position must be inside the kernel.\n\nAutomatically set to center.");
    }

}

void MorphologyDialog::updateUiFromSettings(const Rodbot::Vision::UnitSettings &unitSettings)
{
    Rodbot::Vision::MorphologyTypes::T type = static_cast<Rodbot::Vision::MorphologyTypes::T>(SETTING_TO_INT(unitSettings,type));
    if (type == Rodbot::Vision::MorphologyTypes::EROSION)
        ui->erosionButton->setChecked(true);
    else
        ui->dilationButton->setChecked(true);

    ui->anchorXBox->setValue(SETTING_TO_INT(unitSettings,anchorX));
    ui->anchorYBox->setValue(SETTING_TO_INT(unitSettings,anchorY));
    ui->kernelTypeBox->setCurrentIndex(SETTING_TO_INT(unitSettings,kernelType));
    ui->kernelSizeBox->setValue(SETTING_TO_INT(unitSettings,kernelSize));
    ui->iterationsBox->setValue(SETTING_TO_INT(unitSettings,iterations));
}

void MorphologyDialog::updateSettingsFromDialogs()
{
    validateDialogs();

    if (ui->erosionButton->isChecked())
        settings["type"] = Rodbot::Vision::MorphologyTypes::EROSION;
    else
        settings["type"] = Rodbot::Vision::MorphologyTypes::DILATION;
    settings["kernelSize"] = ui->kernelSizeBox->value();
    settings["kernelType"] = ui->kernelTypeBox->currentIndex();
    settings["anchorX"] = ui->anchorXBox->value();
    settings["anchorY"] = ui->anchorYBox->value();
    settings["iterations"] = ui->iterationsBox->value();

    emit newSettings();

}
