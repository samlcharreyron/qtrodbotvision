#ifndef CONTOURSDIALOG_H
#define CONTOURSDIALOG_H

#include <QWidget>
#include <QMessageBox>
// external
#include <IPChain/IPUnits/Contours/Contours.hpp>
#include <opencv2/imgproc/imgproc.hpp>
//local
#include "ipdialoginterface.h"
#include "units.h"

namespace Ui {
class ContoursDialog;
}

class ContoursDialog : public IPDialog
{
    Q_OBJECT
    Q_INTERFACES(IPDialogInterface)

public:
    explicit ContoursDialog(QWidget *parent = 0);
    virtual ~ContoursDialog();

    static const QString label();

private:
    Ui::ContoursDialog *ui;
    void validateDialogs();
    void updateUiFromSettings(const Rodbot::Vision::UnitSettings &unitSettings);

private slots:
    void updateSettingsFromDialogs();

signals:
    void newSettings();
};

#endif // CONTOURSDIALOG_H
