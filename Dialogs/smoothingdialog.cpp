#include "smoothingdialog.h"
#include "ui_smoothingdialog.h"

SmoothingDialog::SmoothingDialog(QWidget *parent) :
    IPDialog(parent),
    ui(new Ui::SmoothingDialog)
{
    ui->setupUi(this);

    // connect signals and slots
    // buttons
    connect(ui->boxButton,SIGNAL(released()),SLOT(updateSettingsFromDialogs()));
    connect(ui->gaussianButton,SIGNAL(released()),SLOT(updateSettingsFromDialogs()));
    connect(ui->medianButton,SIGNAL(released()),SLOT(updateSettingsFromDialogs()));
    // dialogs
    connect(ui->sizeEdit,SIGNAL(editingFinished()),SLOT(updateSettingsFromDialogs()));
    connect(ui->sxEdit,SIGNAL(editingFinished()),SLOT(updateSettingsFromDialogs()));
    connect(ui->syEdit,SIGNAL(editingFinished()),SLOT(updateSettingsFromDialogs()));
}

SmoothingDialog::~SmoothingDialog()
{
    delete ui;
}

void SmoothingDialog::validateDialogs()
{
    bool inputEmpty = false;
    // If smooth size is EVEN (and not zero), convert to ODD by adding 1
    if(((ui->sizeEdit->text().toInt()%2)==0)&&(ui->sizeEdit->text().toInt()!=0))
    {
        ui->sizeEdit->setText(QString::number(ui->sizeEdit->text().toInt()+1));
        QMessageBox::information(this->parentWidget(),"NOTE:","Size 1 must be an ODD number.\n\nAutomatically set to (inputted value+1).");
    }

    // Check for empty inputs: if empty, set to default values
    if(ui->sizeEdit->text().isEmpty())
    {
        ui->sizeEdit->setText(QString::number(DEFAULT_SMOOTHING_SIZE));
        inputEmpty=true;
    }
    if(ui->sxEdit->text().isEmpty())
    {
        ui->sxEdit->setText(QString::number(DEFAULT_SIGMA_X));
        inputEmpty=true;
    }
    if(ui->syEdit->text().isEmpty())
    {
        ui->syEdit->setText(QString::number(DEFAULT_SIGMA_Y));
        inputEmpty=true;
    }

    // Check for special gaussian cases
    if (ui->gaussianButton->isDown() && ui->sizeEdit->text().toInt() == 0
            && (ui->sxEdit->text().toDouble() == 0.0 || ui->syEdit->text().toDouble() == 0.0))
    {
        ui->sizeEdit->setText(QString::number(DEFAULT_SMOOTHING_SIZE));
        ui->sxEdit->setText(QString::number(DEFAULT_SIGMA_X));
        ui->syEdit->setText(QString::number(DEFAULT_SIGMA_Y));
        QMessageBox::warning(this->parentWidget(),"ERROR:","size and sigma parameters of Gaussian filter cannot be zero.\n\nAutomatically set to defaults.");

    }

    if(inputEmpty)
        QMessageBox::warning(this->parentWidget(),"WARNING:","One or more inputs empty.\n\nAutomatically set to default values.");
}

void SmoothingDialog::updateUiFromSettings(const Rodbot::Vision::UnitSettings& unitSettings)
{

    SmoothingType type = static_cast<SmoothingType>(SETTING_TO_INT(unitSettings,type));
    switch(type) {
    case Rodbot::Vision::SmoothingTypes::BOX: {
        ui->boxButton->setDown(true);
        break;
    }
    case Rodbot::Vision::SmoothingTypes::GAUSSIAN: {
        ui->gaussianButton->setDown(true);
        break;
    }
    case Rodbot::Vision::SmoothingTypes::MEDIAN: {
        ui->medianButton->setDown(true);
        break;
    }
    }

    ui->sizeEdit->setText(SETTING_TO_QSTRING(unitSettings,size));
    ui->sxEdit->setText(SETTING_TO_QSTRING(unitSettings,sigmaX));
    ui->syEdit->setText(SETTING_TO_QSTRING(unitSettings,sigmaY));
}

void SmoothingDialog::updateSettingsFromDialogs()
{
    validateDialogs();

    if (ui->boxButton->isDown()) {
        settings["type"] = Rodbot::Vision::SmoothingTypes::BOX;
    } else if (ui->gaussianButton->isDown()) {
        settings["type"] = Rodbot::Vision::SmoothingTypes::GAUSSIAN;
    } else if (ui->medianButton->isDown()) {
        settings["type"] = Rodbot::Vision::SmoothingTypes::MEDIAN;
    }

    settings["size"] = ui->sizeEdit->text().toInt();
    settings["sigmaX"] = ui->sxEdit->text().toDouble();
    settings["sigmaY"] = ui->syEdit->text().toDouble();

    emit newSettings();
}

const QString SmoothingDialog::label()
{
    return "Smoothing";
}
