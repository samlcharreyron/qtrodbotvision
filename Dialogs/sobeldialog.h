#ifndef SOBELDIALOG_H
#define SOBELDIALOG_H

#include <QWidget>
#include <QMessageBox>
//local
#include "ipdialoginterface.h"
#include "units.h"

namespace Ui {
class SobelDialog;
}

class SobelDialog : public IPDialog
{
    Q_OBJECT
    Q_INTERFACES(IPDialogInterface)

public:
    explicit SobelDialog(QWidget *parent = 0);
    virtual ~SobelDialog();

    static const QString label();

private:
    Ui::SobelDialog *ui;
    void validateDialogs();
    void updateUiFromSettings(const Rodbot::Vision::UnitSettings &unitSettings);

private slots:
    void updateSettingsFromDialogs();
    void setOverlay(bool toOriginal);

signals:
    void newSettings();
};

#endif // SOBELDIALOG_H
