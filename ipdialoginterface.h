#ifndef IPDIALOGINTERFACE_H
#define IPDIALOGINTERFACE_H

#include <QtPlugin>
#include <QMap>
#include <QVariant>
#include <IPChain/UnitTypes/IPUnit.hpp>
//local
//#include "units.h"

class IPDialogInterface
{
public:
    virtual ~IPDialogInterface() {}

    virtual void updateUiFromSettings(const Rodbot::Vision::UnitSettings& unitSettings) = 0;

private:
    virtual void validateDialogs() = 0;

private slots:
    virtual void updateSettingsFromDialogs() = 0;
};

QT_BEGIN_NAMESPACE
Q_DECLARE_INTERFACE(IPDialogInterface,
                    "iris.robot-vision.ipdialog/1.0")
QT_END_NAMESPACE

class IPDialog : public QWidget, public IPDialogInterface {
protected:
    typedef QMap<QString,QVariant> DialogSettings;
    DialogSettings settings;

public:
    IPDialog(QWidget* parent) : QWidget(parent) {}

    const Rodbot::Vision::UnitSettings getSettings() const {
        Rodbot::Vision::UnitSettings outSettings;

        DialogSettings::const_iterator it = settings.begin();
        for (; it != settings.end(); it++) {
            outSettings[it.key().toStdString()] = it.value().toString().toStdString();
        }

        return outSettings;
    }

signals:
    void newSettings();
};

// some useful macros

#define SETTING_TO_QSTRING(settings,s) QString::fromStdString(settings.find(#s)->second)
#define SETTING_TO_INT(settings,s) QString::fromStdString(settings.find(#s)->second).toInt()
#define SETTING_TO_DOUBLE(settings,s) QString::fromStdString(settings.find(#s)->second).toDouble()

#endif // IPDIALOGINTERFACE_H
