#include "chainwindow.h"
#include "ui_chainwindow.h"

ChainWindow::ChainWindow(QWidget *parent, Rodbot::Vision::IPChain *ipChain, Rodbot::Vision::ChainRegistry* chainRegistry) :
    QDialog(parent),
    ui(new Ui::ChainWindow),
    isVertical(true),
    showIOButtons(true)

{
    ui->setupUi(this);
    chainViewer = new ChainViewer(0,ipChain,chainRegistry,ui->listView->getUnitListModel(),isVertical);
    if(isVertical) {
        // flip the width and height of the chain viewer
        resize(height(),width());

        // move the scroll area to second row
        ui->gridLayout->addWidget(chainViewer,3,0);
        ui->listView->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
    } else {
        ui->gridLayout->addWidget(chainViewer,1,2);
    }

    if (showIOButtons) {
        ui->loadConfig->show();
        ui->saveConfig->show();
    } else {
        ui->loadConfig->hide();
        ui->saveConfig->hide();
    }
    connect(ui->listView->getUnitListModel(),SIGNAL(unitDoubleClicked(QMimeData*)),chainViewer,SLOT(addUnit(QMimeData*)));
    connect(ui->loadConfig,SIGNAL(released()),chainViewer,SLOT(loadChainConfig()));
    connect(ui->saveConfig,SIGNAL(released()),chainViewer,SLOT(saveChainConfig()));

}

ChainWindow::~ChainWindow()
{
    delete ui;
    delete chainViewer;
}

void ChainWindow::reloadChainViewer() const
{
    chainViewer->reloadViewerFromChain();
}

