#ifndef CONFIG_H
#define CONFIG_H

#include <QStringList>
#include <QString>
#include <QMap>
#include <QWaitCondition>
//local
//#include "units.h"

// Image buffer size
#define DEFAULT_BUFFER_SIZE                 1
// Drop frame if frame buffer is full
#define DEFAULT_DROP_FRAMES                 false
// Default capture rate
#define DEFAULT_CAPTURE_FPS                 32
// FPS statistics
#define CAPTURE_FPS_STAT_QUEUE_LENGTH       32
#define PROCESSING_FPS_STAT_QUEUE_LENGTH    32
// IP Smoothing settings
#define DEFAULT_SMOOTHING_TYPE              0
#define DEFAULT_SMOOTHING_SIZE              3
#define DEFAULT_SIGMA_X                     1.0
#define DEFAULT_SIGMA_Y                     1.0
// IP Threshold settings
#define DEFAULT_THRESH_TYPE                 0
#define DEFAULT_THRESH_VAL                  127.0
#define DEFAULT_THRESH_MAX_VAL              255.0
// IP Adaptive Threshold settings
#define DEFAULT_ADAPT_THRESH_BLOCK          5
#define DEFAULT_ADAPT_THRESH_C              5
// IP Canny Settings
#define DEFAULT_CANNY_THRESH1               100
#define DEFAULT_CANNY_THRESH2               200
#define DEFAULT_CANNY_APERTURE              3
#define DEFAULT_CANNY_L2                    false
// IP Sobel Settings
#define DEFAULT_SOBEL_DX                    1
#define DEFAULT_SOBEL_DY                    0
#define DEFAULT_SOBEL_KSIZE                 3
// IP Contours Settings
#define DEFAULT_CONTOURS_MODE               0
#define DEFAULT_CONTOURS_METHOD             2
#define DEFAULT_CONTOURS_OFFSET_X           0
#define DEFAULT_CONTOURS_OFFSET_Y           0
#define DEFAULT_CONTOURS_THICKNESS          1
#define DEFAULT_CONTOURS_LINE_TYPE          8
#define DEFAULT_CONTOURS_MAX_LEVEL          0
#define DEFAULT_CONTOURS_SHAPE_OPTIONS      15
#define DEFAULT_CONTOURS_AREA_THRESH        120.0
#define DEFAULT_CONTOURS_EPS_AREA_SF        0.02
#define DEFAULT_CONTOURS_IGNORE_INSIDE      false
// IP Morphology Settings
#define DEFAULT_MORPH_TYPE                  0
#define DEFAULT_MORPH_ANCHOR_X              -1
#define DEFAULT_MORPH_ANCHOR_Y              -1
#define DEFAULT_MORPH_ITERATIONS            1
#define DEFAULT_MORPH_KERNEL_TYPE           0
#define DEFAULT_MORPH_KERNEL_SIZE           3
#define DEFAULT_MORPH_BORDER_TYPE           0

// Drag and Drop
#define IP_UNIT_MIME_TYPE                   "application/x-rodbot-ip-unit"
#define IP_UNIT_CONTAINER_MIME_TYPE         "application/x-ipunitcontainer"
#define UNIT_CONTAINER_DRAG_AREA_FACTOR     0.2

extern QWaitCondition waitForGoodChain;

#endif // CONFIG_H
