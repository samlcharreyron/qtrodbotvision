#include "ipunitcontainer.h"
#include "ui_ipunitcontainer.h"

IPUnitContainer::IPUnitContainer(QWidget* parent, Rodbot::Vision::IPUnit* unit, IPDialog *unitDialog) :
    QFrame(parent),
    ui(new Ui::IPUnitContainer),
    unit(unit),
    drawInsertBar(NONE)
{
    ui->setupUi(this);

    unitName = QString::fromStdString(unit->name());

    ui->unitLabel->setText(unitName);

    // resize container to fit dialog
    int dialogWidth = unitDialog->width();
    this->resize(dialogWidth,this->height());

    this->unitDialog = unitDialog;
    ui->verticalLayout->addWidget(unitDialog);
    //ui->verticalLayout->addStretch(1);

    connect(ui->defaultsButton,SIGNAL(released()),SLOT(resetDefaults()));
    connect(ui->removeButton,SIGNAL(released()),SIGNAL(removeUnitRequested()));
    connect(ui->enabledBox,SIGNAL(toggled(bool)),SIGNAL(enableChanged(bool)));
    connect(ui->minimizeButton,SIGNAL(released()),SLOT(toggleMinimize()));
    connect(unitDialog,SIGNAL(newSettings()),SIGNAL(settingsChanged()));

    connect(ui->moveUpButton,SIGNAL(released()),SLOT(requestMoveUp()));
    connect(ui->moveDownButton,SIGNAL(released()),SLOT(requestMoveDown()));
}

void IPUnitContainer::resetDefaults()
{
    unit->resetDefault();
    unitDialog->updateUiFromSettings(unit->getSettings());
}

void IPUnitContainer::toggleMinimize()
{
    if(unitDialog->isVisible()) {
        unitDialog->hide();
        ui->minimizeButton->setArrowType(Qt::UpArrow);
    } else {
        unitDialog->show();
        ui->minimizeButton->setArrowType(Qt::DownArrow);
    }
}

void IPUnitContainer::requestMoveUp()
{
    emit(moveRequested(true));
}

void IPUnitContainer::requestMoveDown()
{
    emit(moveRequested(false));
}

IPUnitContainer::~IPUnitContainer()
{
    delete unitDialog;
    delete ui;
}

Rodbot::Vision::IPUnit *IPUnitContainer::getUnit() const
{
    return unit;
}

void IPUnitContainer::paintEvent(QPaintEvent *e)
{
    QFrame::paintEvent(e);
    int dragAreaHeight = UNIT_CONTAINER_DRAG_AREA_FACTOR*height();
    if(drawInsertBar == TOP) {
//        QPainter p(this);
//        p.setBrush(Qt::red);
//        p.setPen(Qt::NoPen);
//        QRect topRect(this->frameGeometry().topLeft(),
//                      QPoint(this->frameGeometry().right(),this->frameGeometry().top()+10));
//        p.drawRect(topRect);
    } else if (drawInsertBar == BOTTOM) {
//        QPainter p(this);
//        p.setBrush(Qt::black);
//        p.setPen(Qt::NoPen);
//        QRect bottomRect(QPoint(this->frameGeometry().left(),this->frameGeometry().bottom()+10),
//                         this->frameGeometry().bottomRight());
//        QRect bottomDragArea(frameGeometry().left(),frameGeometry().bottom() + 0.5*dragAreaHeight,
//                             width(),dragAreaHeight);
//        p.drawRect(bottomRect);
    }

    // reset so it doesnt stick
    drawInsertBar = NONE;
}

int IPUnitContainer::openCVErrorHandler(int status, const char *func_name, const char *err_msg, const char *file_name, int line, void *userdata)
{
    QString errorMsg;
    QTextStream stream(&errorMsg);
    stream << "The following OpenCV error was encountered in line " <<  line << " of function " << *func_name  << " in file " << *file_name << " " << *err_msg;
    QMessageBox::critical(0,"OpenCV Error", errorMsg  );

    removeUnitRequested();
}

IPUnitContainer::InsertPositions IPUnitContainer::atBoundary(const QRect pos) const
{
    int dragAreaHeight = UNIT_CONTAINER_DRAG_AREA_FACTOR*height();
    QRect topDragArea = QRect(frameGeometry().left(),frameGeometry().top(),
                              width(),dragAreaHeight);
    QRect bottomDragArea = QRect(frameGeometry().left(),frameGeometry().bottom() - dragAreaHeight,
                                 width(),dragAreaHeight);

    if (pos.intersects(topDragArea)) {
        return TOP;
    }
    else if (pos.intersects(bottomDragArea)) {
        return BOTTOM;
    } else {
        return NONE;
    }
}

void IPUnitContainer::setMoveButtonHidden(bool hidden, int button)
{
    if ((button & UP_BUTTON) > 0) {
        ui->moveUpButton->setHidden(hidden);
    }

    if((button & DOWN_BUTTON) > 0) {
        ui->moveDownButton->setHidden(hidden);
    }
}

