#ifndef MAINWINDOW_H
#define MAINWINDOW_H

// Qt
#include <QMainWindow>
#include <QMessageBox>
#include <QFileDialog>
#include <QDebug>
#include <QMap>
// Local
#include "Buffer.h"
#include "videoviewer.h"
#include "chainwindow.h"
#include "config.h"
#include "units.h"
#include <IPChain/IPChain.hpp>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    Buffer<Mat>* buffer;
    VideoViewer* videoViewer;
    ChainWindow* chainWindow;
    Rodbot::Vision::IPChain *ipChain;
    Rodbot::Vision::ChainRegistry *chainRegistry;

public slots:
    void openVideoFile();
    void closeVideoFile();
    void showAboutDialog();
    void showChainWindow(bool isChecked);

private slots:
    void uncheckChainWindowMenu();
    void loadIPChainFromFile();
    void saveIPChainToFile();

signals:
    void chainSettingsLoaded();
};

#endif // MAINWINDOW_H
