#ifndef PROCESSINGTHREAD_H
#define PROCESSINGTHREAD_H

// Qt
#include <QtCore/QTime>
#include <QtCore/QQueue>
#include <QWaitCondition>
// OpenCV
#include <opencv2/highgui/highgui.hpp>
// Local
#include "config.h"
#include "Buffer.h"
#include "MatToQImage.h"
#include "structures.h"
#include <IPChain/IPChain.hpp>

class Processing : public QObject
{
    Q_OBJECT
public:
    explicit Processing(Buffer<Mat>* buffer, Rodbot::Vision::IPChain* ipChain);
    void setDoStop(const bool stop);

private slots:
    void processFrame();

private:
    void updateFPS(int);
    Buffer<Mat> *buffer;
    Mat          currentFrame;
    Mat          currentFrameGrayscale;
    Mat          processedFrame;
    QImage       frame;
    QTime   t;
    QQueue<int> fps;
    QMutex doStopMutex;
    QMutex processingMutex;
    Size frameSize;
    Point framePoint;
    Rodbot::Vision::IPChain* ipChain;
    struct ThreadStatisticsData statsData;
    volatile bool doStop;
    int processingTime;
    int fpsSum;
    int sampleNumber;
    bool enableFrameProcessing;

signals:
    void newFrame(const QImage &frame);
    void updateStatisticsInGUI(struct ThreadStatisticsData);
    void errorInUnit(Rodbot::Vision::IPUnit* unit);

};

#endif // PROCESSINGTHREAD_H
