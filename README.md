# QT Rodbot IP

1. Prerequisites
2. Building
3. Installing
4. Contributing
5. Acknowledgements

## 1. Prerequisites

This project requires:

* Qt version 4 or later
* Rodbot IPChain Library

## 2. Building
Open qt-robot-ip.pro in QtCreator and build.  Right click on project and click Add Library to add the reference to the IPChain library.  Build or debug in QtCreator.

## 3. Installing
Todo: Complete section

## 4. Contributing
Contact samlcharreyron@gmail.com for info on how you can help.

## 5. Acknowledgements
This project was initially based on Nick Dademo's [multithreaded front-end for OpenCV](https://code.google.com/p/qt-opencv-multithreaded/).  Most of the functionality has been removed but some of his code remains...
