#ifndef IPUNITCONTAINER_H
#define IPUNITCONTAINER_H

#include <QFrame>
#include <QDebug>
#include <QPainter>
#include <QSpacerItem>
#include <QSignalMapper>
#include <QMessageBox>
//local
#include "ipdialoginterface.h"
#include "units.h"
#include "config.h"

namespace Ui {
class IPUnitContainer;
}

class IPUnitContainer : public QFrame
{
    Q_OBJECT

public:

    explicit IPUnitContainer(QWidget* parent, Rodbot::Vision::IPUnit* unit, IPDialog* unitDialog);
    ~IPUnitContainer();

    Rodbot::Vision::IPUnit *getUnit() const;

    const Rodbot::Vision::UnitSettings getSettings() const {
        return this->unitDialog->getSettings();
    }

    enum InsertPositions {
        TOP,
        BOTTOM,
        NONE
    };

    enum MoveButtons {
        UP_BUTTON   = 0x1,
        DOWN_BUTTON = 0x2
    };

    InsertPositions drawInsertBar;

    QString unitName;

    InsertPositions atBoundary(const QRect pos) const;

    // hide move button (if at top already for example)
    // parameter is up button or else down button
    void setMoveButtonHidden(bool hidden, int button);

protected:
    void paintEvent(QPaintEvent *e);

    int openCVErrorHandler( int status, const char* func_name, const char* err_msg,
                            const char* file_name, int line, void* userdata);

private:
    Ui::IPUnitContainer *ui;
    IPDialog *unitDialog;
    Rodbot::Vision::IPUnit *unit;

signals:
    void removeUnitRequested();
    void enableChanged(bool enabled);
    void settingsChanged();
    void moveRequested(bool);
public slots:


private slots:
     void resetDefaults();
     void toggleMinimize();
     void requestMoveUp();
     void requestMoveDown();

};

#endif // IPUNITCONTAIER_H
