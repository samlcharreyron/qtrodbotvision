#include "capture.h"

Capture::Capture(Buffer<Mat> *buffer, QString fileName, bool dropFrameIfBufferFull, int width, int height) : buffer(buffer)
{
    // Save passed parameters
    this->dropFrameIfBufferFull=dropFrameIfBufferFull;
    this->fileName=fileName;
    this->width = width;
    this->height = height;
    this->fps = DEFAULT_CAPTURE_FPS;
    // Initialize variables(s)
    doStop=false;
    sampleNumber=0;
    fpsSum=0;
    //fps.clear();
    statsData.averageFPS=0;
    statsData.nFramesProcessed=0;
}

bool Capture::connectToFile()
{
    // Open camera
    bool camOpenResult = cap.open(fileName.toStdString());
    this->fps = cap.get(CV_CAP_PROP_FPS);
    playbackState = PLAYING;
    // Set resolution
    if(width != -1)
        cap.set(CV_CAP_PROP_FRAME_WIDTH, width);
    if(height != -1)
        cap.set(CV_CAP_PROP_FRAME_HEIGHT, height);
    // Return result

    // Start timer (used to set capture rate)
    t.start(1000/fps);

    // connect signal and slots
    connect(&t,SIGNAL(timeout()),this,SLOT(captureFrame()));

    return camOpenResult;
}

bool Capture::disconnectFile()
{
    playbackState = STOPPED;
    t.stop();
    // Camera is connected
    if(cap.isOpened())
    {
        // Disconnect camera
        cap.release();
        return true;
    }
    // Camera is NOT connected
    else
        return false;
}

void Capture::captureFrame()
{
    if (playbackState == PLAYING) {
        //Capture frame (if finished loop back to begininng)
        if (!cap.grab()) {
            cap.open(fileName.toStdString());
            cap.grab();
        }

        // Retrieve frame
        cv::Mat colorFrame;
        cap.retrieve(colorFrame);
        if (colorFrame.channels() == 3)
            cv::cvtColor(colorFrame,grabbedFrame,CV_RGB2GRAY);
        else
            grabbedFrame = colorFrame;
    }

    // Add frame to buffer
    buffer->add(grabbedFrame, dropFrameIfBufferFull);

    // Inform that new frame is pushed to buffer
    emit newFrame();

    statsData.averageFPS = fps;
    statsData.nFramesProcessed++;

    // Inform GUI of updated statistics
    emit updateStatisticsInGUI(statsData);
}

bool Capture::isFileConnected()
{
    return cap.isOpened();
}

int Capture::getInputSourceWidth()
{
    return cap.get(CV_CAP_PROP_FRAME_WIDTH);
}

int Capture::getInputSourceHeight()
{
    return cap.get(CV_CAP_PROP_FRAME_HEIGHT);
}

void Capture::updateFps(int fps)
{
    if (isFileConnected()) {
        this->fps = fps;
        t.setInterval(1000/fps);
    }
}

void Capture::startPlayback()
{
    if (playbackState == STOPPED)
        connectToFile();
    else
        playbackState = PLAYING;
}

void Capture::pausePlayback()
{
    playbackState = PAUSED;
}

void Capture::stopPlayback()
{
    disconnectFile();
}
