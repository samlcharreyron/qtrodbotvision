#ifndef UNITLIST_H
#define UNITLIST_H

#include <QListView>
#include <QtGui>
//local
#include "config.h"
#include "registrar.h"
#include "units.h"

class UnitListModel : public QAbstractListModel, public Registrar
{
    Q_OBJECT
public:
    explicit UnitListModel(QObject *parent=0);
    int rowCount(const QModelIndex &) const;
    QVariant data(const QModelIndex &index, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;

    // returns true if IPChain unit has an associated dialog
    bool hasDialog(const QString& unitName) const;
    // returns the dialog name associated with IPChain unit name
    const QString getDialogName(const QString& unitName) const;

protected:
    //Qt::DropActions supportedDropActions() const;
    QStringList mimeTypes() const;
    QMimeData* mimeData(const QModelIndexList &indexes) const;

signals:
    void unitDoubleClicked(QMimeData* mimeData);

public slots:
    void handleDoubleClick(QModelIndex index);

};

class UnitListView : public QListView
{
    Q_OBJECT
    Q_PROPERTY(UnitListModel* unitListModel READ getUnitListModel)

    UnitListModel* unitListModel;
public:
    explicit UnitListView(QWidget *parent = 0,UnitListModel *model=new UnitListModel());
    UnitListModel* getUnitListModel() const;

protected:
    void dragEnterEvent(QDragEnterEvent *event);
    void dragMoveEvent(QDragMoveEvent *e);
    void dragLeaveEvent(QDragLeaveEvent *e);

};

#endif // UNITLIST_H
