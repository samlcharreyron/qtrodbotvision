#include "videoviewer.h"
#include "ui_videoviewer.h"

VideoViewer::VideoViewer() : ui(new Ui::VideoViewer)
{
    this->fileConnected = false;
}

VideoViewer::VideoViewer(QWidget *parent, Buffer<Mat>* buffer, Rodbot::Vision::IPChain *ipChain, ChainViewer *chainViewer) :
    QWidget(parent),
    ui(new Ui::VideoViewer)
{
    ui->setupUi(this);

    this->buffer = buffer;
    this->ipChain = ipChain;
    this->fileConnected = false;
    this->chainViewer = chainViewer;

    // setup UI
    ui->frameLabel->setText("No camera connected.");
    ui->captureRateLabel->setText("");
    ui->processingRateLabel->setText("");
    ui->speedSlider->setValue(DEFAULT_CAPTURE_FPS);

    // connect signals/slots
    connect(ui->speedSlider,SIGNAL(valueChanged(int)),SLOT(updateFps(int)));

    qRegisterMetaType<struct ThreadStatisticsData>("ThreadStatisticsData");
}

VideoViewer::~VideoViewer()
{
    delete ui;
    if (fileConnected) {
        closeFile();
    }
}

bool VideoViewer::connectToFile(QString fileName, bool dropFrame, int width, int height)
{
    if (fileConnected) {
        closeFile();
    }

    capture = new Capture(buffer,fileName,dropFrame,width,height);
    captureThread = new QThread();
    capture->moveToThread(captureThread);
    if (capture->connectToFile()) {
        fileConnected = true;
        processing = new Processing(buffer,ipChain);
        processingThread = new QThread();
        processing->moveToThread(processingThread);

        connect(capture,SIGNAL(newFrame()),processing,SLOT(processFrame()));
        connect(processing,SIGNAL(newFrame(const QImage&)),this, SLOT(updateFrame(const QImage&)));

        connect(processing, SIGNAL(updateStatisticsInGUI(struct ThreadStatisticsData)), this, SLOT(updateProcessingThreadStats(struct ThreadStatisticsData)));
        connect(capture, SIGNAL(updateStatisticsInGUI(struct ThreadStatisticsData)), this, SLOT(updateCaptureThreadStats(struct ThreadStatisticsData)));
        //automatically delete thread and task object when work is done:
        connect( this,SIGNAL(captureFinished()), capture, SLOT(deleteLater()) );
        connect( this,SIGNAL(processingFinished()), processing, SLOT(deleteLater()) );

        // playback buttons
        connect(ui->playButton,SIGNAL(released()),capture,SLOT(startPlayback()));
        connect(ui->pauseButton,SIGNAL(released()),capture,SLOT(pausePlayback()));
        connect(ui->stopButton,SIGNAL(released()),capture,SLOT(stopPlayback()));

        // error handling
        connect(processing, SIGNAL(errorInUnit(Rodbot::Vision::IPUnit*)), chainViewer, SLOT(handleUnitError(Rodbot::Vision::IPUnit*)));

        captureThread->start();
        processingThread->start();

        return true;
    } else {
        QMessageBox::warning(this,"ERROR:","Could not connect to file");
        return false;
    }
}

void VideoViewer::closeFile()
{
    if (fileConnected) {
        stopProcessing();
        qDebug() << "Processing thread stopped";
        stopCapture();
        qDebug() << "Capture thread stopped";
        ui->frameLabel->clear();
        fileConnected = false;
        qDebug() << "file closed";
    }
}

bool VideoViewer::isfileConnected() const
{
    return fileConnected;
}

void VideoViewer::stopCapture()
{
    qDebug() << "About to stop capture thread...";
    capture->disconnectFile();
    // Take one frame off a FULL queue to allow the capture thread to finish
    if(buffer->isFull())
        buffer->get();
    emit(captureFinished());
}

void VideoViewer::stopProcessing()
{
    qDebug() << "About to stop processing thread...";
    emit(processingFinished());
}

void VideoViewer::clearImageBuffer()
{
    if(buffer->clear())
        qDebug() << "Image buffer successfully cleared.";
    else
        qDebug() << "WARNING: Could not clear image buffer.";
}

void VideoViewer::updateCaptureThreadStats(struct ThreadStatisticsData statData)
{
    // Show processing rate in captureRateLabel
    ui->captureRateLabel->setText(QString::number(statData.averageFPS)+" fps");
    // Show number of frames captured in nFramesCapturedLabel
    ui->nFramesCapturedLabel->setText(QString("[") + QString::number(statData.nFramesProcessed) + QString("]"));
}

void VideoViewer::updateFps(int fps)
{
    if (fileConnected)
        capture->updateFps(fps);
}

void VideoViewer::updateProcessingThreadStats(struct ThreadStatisticsData statData)
{
    // Show processing rate in processingRateLabel
    ui->processingRateLabel->setText(QString::number(statData.averageFPS)+" fps");

    // Show number of frames processed in nFramesProcessedLabel
    ui->nFramesProcessedLabel->setText(QString("[") + QString::number(statData.nFramesProcessed) + QString("]"));
}


void VideoViewer::updateFrame(const QImage &frame)
{
    // Display frame
    ui->frameLabel->setPixmap(QPixmap::fromImage(frame));
}
