#ifndef MATTOQIMAGE_H
#define MATTOQIMAGE_H

// Qt
#include <QtGui/QImage>
// OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;

QImage MatToQImage(const Mat&);

#endif // MATTOQIMAGE_H

